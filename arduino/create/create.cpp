#include "create.h"
#include <timer.h>

//
// Make sure the structs defined in the header file is sane. 
//
#define C_ASSERT(e) typedef char __C_ASSERT__[(e)?1:-1]
C_ASSERT(sizeof(create_data_group1) == 10);
C_ASSERT(sizeof(create_data_group2) == 6);
C_ASSERT(sizeof(create_data_group3) == 10);
C_ASSERT(sizeof(create_data_group4) == 14);
C_ASSERT(sizeof(create_data_group5) == 12);
C_ASSERT(sizeof(create_data_t) == 52);

C_ASSERT(create_packet_group1_count <= 8);
C_ASSERT(create_packet_group2_count <= 8);
C_ASSERT(create_packet_group3_count <= 8);
C_ASSERT(create_packet_group4_count <= 8);
C_ASSERT(create_packet_group5_count <= 8);

C_ASSERT(sizeof(create_led_t) == 3);

//
// Here is a table of all possible iRobot Create Open Interface v2 packets.
//
typedef struct create_packet_table_entry {
    uint8_t offset;                // offset into the create_data_t struct
    uint8_t length;                // length of the packet
    create_packet_mask_t mask;    // contained fields
} create_packet_table_entry_t;

const create_packet_table_entry_t create_packet_table[] = {
    // 0 - 6
    {0, 26, {0xff, 0xff, 0xff, 0, 0}},
    {0, 10, {0xff, 0, 0, 0, 0}},
    {10, 6, {0, 0xff, 0, 0, 0}},
    {16, 10, {0, 0, 0xff, 0, 0}},
    {26, 14, {0, 0, 0, 0xff, 0}},
    {40, 12, {0, 0, 0, 0, 0xff}},
    {0, 52, {0xff, 0xff, 0xff, 0xff, 0xff}},
    // 7 - 16
    {0, 1, {CREATE_PACKET_MASK(1, bumps_and_wheel_drops), 0, 0, 0, 0}},
    {1, 1, {CREATE_PACKET_MASK(1, wall), 0, 0, 0, 0}},
    {2, 1, {CREATE_PACKET_MASK(1, cliff_left), 0, 0, 0, 0}},
    {3, 1, {CREATE_PACKET_MASK(1, cliff_front_left), 0, 0, 0, 0}},
    {4, 1, {CREATE_PACKET_MASK(1, cliff_front_right), 0, 0, 0, 0}},
    {5, 1, {CREATE_PACKET_MASK(1, cliff_right), 0, 0, 0, 0}},
    {6, 1, {CREATE_PACKET_MASK(1, virtual_wall), 0, 0, 0, 0}},
    {7, 1, {CREATE_PACKET_MASK(1, overcurrents), 0, 0, 0, 0}},
    {8, 1, {0, 0, 0, 0, 0}},
    {9, 1, {0, 0, 0, 0, 0}},
    // 17 - 20
    {10, 1, {0, CREATE_PACKET_MASK(2, ir_byte), 0, 0, 0}},
    {11, 1, {0, CREATE_PACKET_MASK(2, buttons), 0, 0, 0}},
    {12, 2, {0, CREATE_PACKET_MASK(2, distance), 0, 0, 0}},
    {14, 2, {0, CREATE_PACKET_MASK(2, angle), 0, 0, 0}},
    // 21 - 26
    {16, 1, {0, 0, CREATE_PACKET_MASK(3, charging_state), 0, 0}},
    {17, 2, {0, 0, CREATE_PACKET_MASK(3, voltage), 0, 0}},
    {19, 2, {0, 0, CREATE_PACKET_MASK(3, current), 0, 0}},
    {21, 1, {0, 0, CREATE_PACKET_MASK(3, battery_temperature), 0, 0}},
    {22, 2, {0, 0, CREATE_PACKET_MASK(3, battery_charge), 0, 0}},
    {24, 2, {0, 0, CREATE_PACKET_MASK(3, battery_capacity), 0, 0}},
    // 27 - 34
    {26, 2, {0, 0, 0, CREATE_PACKET_MASK(4, wall_signal), 0}},
    {28, 2, {0, 0, 0, CREATE_PACKET_MASK(4, cliff_left_signal), 0}},
    {30, 2, {0, 0, 0, CREATE_PACKET_MASK(4, cliff_front_left_signal), 0}},
    {32, 2, {0, 0, 0, CREATE_PACKET_MASK(4, cliff_front_right_signal), 0}},
    {34, 2, {0, 0, 0, CREATE_PACKET_MASK(4, cliff_right_signal), 0}},
    {36, 1, {0, 0, 0, CREATE_PACKET_MASK(4, user_digital_inputs), 0}},
    {37, 2, {0, 0, 0, CREATE_PACKET_MASK(4, user_analog_input), 0}},
    {39, 1, {0, 0, 0, CREATE_PACKET_MASK(4, charging_sources_available), 0}},
    // 35 - 42
    {40, 1, {0, 0, 0, 0, CREATE_PACKET_MASK(5, oi_mode)}},
    {41, 1, {0, 0, 0, 0, CREATE_PACKET_MASK(5, song_number)}},
    {42, 1, {0, 0, 0, 0, CREATE_PACKET_MASK(5, song_playing)}},
    {43, 1, {0, 0, 0, 0, CREATE_PACKET_MASK(5, number_of_stream_packets)}},
    {44, 2, {0, 0, 0, 0, CREATE_PACKET_MASK(5, velocity)}},
    {46, 2, {0, 0, 0, 0, CREATE_PACKET_MASK(5, radius)}},
    {48, 2, {0, 0, 0, 0, CREATE_PACKET_MASK(5, right_velocity)}},
    {50, 2, {0, 0, 0, 0, CREATE_PACKET_MASK(5, left_velocity)}}
};
C_ASSERT(sizeof(create_packet_table) == (sizeof(create_packet_table_entry_t) * (CREATE_PACKET_ID_MAX + 1)));

CreateRobot Create;

CreateRobot::CreateRobot() :
    mode(create_mode_off),
    mode_requested(create_mode_off),
    mode_timestamp(0),
    serial(0),
    packet_state(create_packet_state_header),
    packet_length(0),
    packet_index(0),
    packet_checksum(0),
    data_timestamp(0),
    data_callback(0),
    data_context(0),
    power(false),
    power_requested(false),
    power_status_pin(0),
    power_toggle_pin(0),
    power_status_timestamp(0),
    power_toggle_timestamp(0)
{
    memset(&packet_data, 0, sizeof(packet_data));
    memset(&data, 0, sizeof(data));
}

void CreateRobot::setup(uint8_t power_status_pin, uint8_t power_toggle_pin, HardwareSerial *serial)
{
    this->serial = serial;
    this->power_status_pin = power_status_pin;
    this->power_toggle_pin = power_toggle_pin;

    serial->begin(57600);
    
    pinMode(power_status_pin, INPUT);
    pinMode(power_toggle_pin, OUTPUT);
    digitalWrite(power_toggle_pin, LOW);
}

void CreateRobot::ondata(create_data_callback_t data_callback, void *data_context)
{
    this->data_callback = data_callback;
    this->data_context = data_context;
}

void CreateRobot::run()
{
    this->control_power();
    this->read_packet();
    this->control_mode();
    this->drive_safety();
}

void CreateRobot::off()
{
    this->mode_requested = create_mode_off;
    this->power_requested = false;
}

void CreateRobot::passive()
{
    this->mode_requested = create_mode_passive;
    this->power_requested = true;
}

void CreateRobot::safe()
{
    this->mode_requested = create_mode_safe;
    this->power_requested = true;
}

void CreateRobot::full()
{
    this->mode_requested = create_mode_full;
    this->power_requested = true;
}

void CreateRobot::drive(int16_t speed_left, int16_t speed_right)
{
    if (!this->power)
    {
        return;
    }

    if (this->mode != create_mode_safe && this->mode != create_mode_full)
    {
        return;
    }

    this->serial->write(145);
    this->serial->write(speed_right >> 8);
    this->serial->write(speed_right & 0xff);
    this->serial->write(speed_left >> 8);
    this->serial->write(speed_left & 0xff);
    this->drive_timestamp = Timer.getTickCount();
}

void CreateRobot::led(bool advance, bool play, uint8_t power_color, uint8_t power_intensity)
{
    static create_led_t led = {0, 0, 0};
    
    if (!this->power)
    {
        return;
    }
    
    if (this->mode != create_mode_safe && this->mode != create_mode_full)
    {
        return;
    }

    led.s.advance_and_play.s.advance = advance;
    led.s.advance_and_play.s.play = play;
    led.s.power_color = power_color;
    led.s.power_intensity = power_intensity;

    this->serial->write(139);
    this->serial->write(led.u8, sizeof(create_led_t));
}

const create_data_t &CreateRobot::get() const
{
    return this->data;
}

void CreateRobot::read_packet()
{
    int data = 0;
    uint8_t byte = 0;
    uint8_t index = 0;

    for (;;)
    {
        data = serial->read();
        if (data < 0)
        {
            break;
        }
        byte = (uint8_t)(data & 0xffu);

        switch (this->packet_state)
        {
        case create_packet_state_header:
            if (byte == CREATE_PACKET_HEADER)
            {
                this->packet_checksum = byte;
                this->packet_state = create_packet_state_length;
            }
            break;

        case create_packet_state_length:
            this->packet_checksum += byte;
            this->packet_length = byte;
            this->packet_index = 0;
            if (!this->packet_length)
            {
                // invalid packet with zero length body
                this->packet_state = create_packet_state_checksum;
            }
            else
            {
                this->packet_state = create_packet_state_data;
            }
            break;

        case create_packet_state_data:
            this->packet_checksum += byte;
            this->packet_data[this->packet_index] = byte;
            ++this->packet_index;
            if (this->packet_index == this->packet_length)
            {
                this->packet_state = create_packet_state_checksum;
            }
            break;

        case create_packet_state_checksum:
            this->packet_checksum += byte;
            if (this->packet_length && !this->packet_checksum)
            {
                this->process_packet();
            }
            this->packet_state = create_packet_state_header;
            break;

        default:
            this->packet_state = create_packet_state_header;
            break;
        }
    }
}

void CreateRobot::process_packet()
{
    uint8_t index = 0;
    uint8_t id = 0;
    uint8_t temp = 0;
    const create_packet_table_entry_t *entry = 0;
    create_packet_mask_t mask = {0, 0, 0, 0, 0};
    create_mode_t mode = create_mode_off;

    if (!this->packet_length)
    {
        return;
    }

    while(index < this->packet_length)
    {
        id = this->packet_data[index];
        if (id > CREATE_PACKET_ID_MAX)
        {
            // invalid packet id
            return;
        }

        entry = &create_packet_table[id];

        if (index + 1 + entry->length > this->packet_length)
        {
            // don't have enough data
            return;
        }

        memcpy(&this->data.u8[entry->offset], &this->packet_data[index + 1], entry->length);

        mask.group1 |= entry->mask.group1;
        mask.group2 |= entry->mask.group2;
        mask.group3 |= entry->mask.group3;
        mask.group4 |= entry->mask.group4;
        mask.group5 |= entry->mask.group5;
        
        index += entry->length + 1;
    }

    // swap endian
    for (id = 7; id <= CREATE_PACKET_ID_MAX; id++)
    {
        entry = &create_packet_table[id];
        if (entry->length != 2)
        {
            continue;
        }

        if ((entry->mask.group1 & mask.group1) ||
            (entry->mask.group2 & mask.group2) ||
            (entry->mask.group3 & mask.group3) ||
            (entry->mask.group4 & mask.group4) ||
            (entry->mask.group5 & mask.group5))
        {
            temp = this->data.u8[entry->offset];
            this->data.u8[entry->offset] = this->data.u8[entry->offset + 1];
            this->data.u8[entry->offset + 1] = temp;
        }
    }

    // if data contains oi mode
    if (CREATE_PACKET_MASK_TEST(mask, 5, oi_mode))
    {
        switch(this->data.s.g5.s.oi_mode)
        {
        case 1:
            mode = create_mode_passive;
            break;
        case 2:
            mode = create_mode_safe;
            break;
        case 3:
            mode = create_mode_full;
            break;
        case 0:
        default:
            mode = create_mode_off;
            break;
        }

        if (mode != this->mode)
        {
            this->mode = mode;
            this->switch_mode();
        }

        this->data_timestamp = Timer.getTickCount();
    }

    if (this->data_callback)
    {
        this->data_callback(this->data_context, &this->data, &mask);
    }
}

void CreateRobot::control_power()
{
    // determine power status
    if (Timer.getTickCount() - this->power_status_timestamp > (uint32_t)100)
    {
        if (digitalRead(this->power_status_pin) == LOW)
        {
            if (this->power)
            {
                // a transition from power on to off
                // need to restore toggle pin to low
                digitalWrite(this->power_toggle_pin, LOW);
                this->power_toggle_timestamp = Timer.getTickCount();
                this->power = false;
            }

            if (this->mode != create_mode_off)
            { 
                this->mode = create_mode_off;
                this->switch_mode();
            }
        }
        else
        {
            if (!this->power)
            {
                // turning on
                // need to restore toggle pin to low
                digitalWrite(this->power_toggle_pin, LOW);
                this->power_toggle_timestamp = Timer.getTickCount();
                this->power = true;
                
                // when robot turns on always starts out from off mode
                if (this->mode != create_mode_off)
                {
                    this->mode = create_mode_off;
                    this->switch_mode();
                }
            }
        }
        this->power_status_timestamp = Timer.getTickCount();
    }

    // turning on/off
    if (this->power != this->power_requested)
    {
        if (digitalRead(this->power_toggle_pin) == LOW)
        {
            // after 250 ms of staying low, we can set toggle pin to high
            if (Timer.getTickCount() - this->power_toggle_timestamp > (uint32_t)250)
            {
                digitalWrite(this->power_toggle_pin, HIGH);
                this->power_toggle_timestamp = Timer.getTickCount();
                this->power_status_timestamp = Timer.getTickCount();
            }
        }
        else
        {
            // after 50 ms of staying high, we can set toggle pin back to low
            if (Timer.getTickCount() - this->power_toggle_timestamp > (uint32_t)50)
            {
                digitalWrite(this->power_toggle_pin, LOW);
                this->power_toggle_timestamp = Timer.getTickCount();
            }
        }
    }
}

void CreateRobot::control_mode()
{
    if (Timer.getTickCount() - this->mode_timestamp > (uint32_t)1000)
    {
        if (this->power &&
            this->power_requested &&
            this->mode != this->mode_requested &&
            this->mode_requested != create_mode_off)
        {
            if (this->mode == create_mode_off)
            {
                // start command
                this->serial->write(128);

                // start streaming sensor data
                this->serial->write(148);
                this->serial->write(1);
                this->serial->write(6);
            }
            else
            {
                // switching mode
                switch(this->mode_requested)
                {
                case create_mode_passive:
                    this->serial->write(128);
                    break;

                case create_mode_safe:
                    this->serial->write(131);
                    break;

                case create_mode_full:
                    this->serial->write(132);
                    break;
                }
            }
        }
        this->mode_timestamp = Timer.getTickCount();
    }

    // make sure data is streaming
    if (Timer.getTickCount() - this->data_timestamp > (uint32_t)500)
    {
        if (this->power &&
            this->power_requested &&
            this->mode != create_mode_off &&
            this->mode_requested != create_mode_off)
        {
            // start streaming sensor data
            this->serial->write(148);
            this->serial->write(1);
            this->serial->write(6);
        }
        this->data_timestamp = Timer.getTickCount();
    }
}

void CreateRobot::switch_mode()
{
    switch(this->mode)
    {
    case create_mode_off:
        break;

    case create_mode_passive:
        break;

    case create_mode_safe:
        this->led(false, true, 0, 128);
        break;

    case create_mode_full:
        this->led(true, false, 128, 128);
        break;
    }
}

void CreateRobot::drive_safety()
{
    static uint8_t stop_command[5] = {145, 0, 0, 0, 0};

    if (Timer.getTickCount() - this->drive_timestamp > (uint32_t)1000)
    {
        //
        // Stop motors after timeout.
        //
        if (this->power &&
            this->power_requested &&
            this->mode != create_mode_off &&
            this->mode_requested != create_mode_off)
        {
            this->serial->write(stop_command, 5);
        }
        this->drive_timestamp = Timer.getTickCount();
    }
}
