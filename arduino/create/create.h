#ifndef _CREATE_H_
#define _CREATE_H_

#include <Arduino.h>

typedef enum create_mode
{
    create_mode_off = 0,
    create_mode_passive,
    create_mode_safe,
    create_mode_full
} create_mode_t;

typedef enum create_packet_state
{
    create_packet_state_header = 0,
    create_packet_state_length,
    create_packet_state_data,
    create_packet_state_checksum
} create_packet_state_t;

typedef union create_data_group1
{
    struct
    {
        union
        {
            struct
            {
                uint8_t bump_right : 1;
                uint8_t bump_left : 1;
                uint8_t wheel_drop_right : 1;
                uint8_t wheel_drop_left : 1;
                uint8_t wheel_drop_caster : 1;
                uint8_t _reserved : 3;
            } s;
            uint8_t u8;
        } bumps_and_wheel_drops;
        uint8_t wall;
        uint8_t cliff_left;
        uint8_t cliff_front_left;
        uint8_t cliff_front_right;
        uint8_t cliff_right;
        uint8_t virtual_wall;
        union
        {
            struct
            {
                uint8_t low_side_driver_1 : 1;
                uint8_t low_side_driver_0 : 1;
                uint8_t low_side_driver_2 : 1;
                uint8_t right_wheel : 1;
                uint8_t left_wheel : 1;
                uint8_t _reserved : 3;
            } s;
            uint8_t u8;
        } overcurrents;
        uint8_t _reserved[2];
    } s;
    uint8_t u8[10];
} create_data_group1_t;

typedef union create_data_group2
{
    struct
    {
        uint8_t ir_byte;
        union
        {
            struct
            {
                uint8_t play : 1;
                uint8_t _reserved_0 : 1;
                uint8_t advance : 1;
                uint8_t _reserved_1 : 5;
            } s;
            uint8_t u8;
        } buttons;
        int16_t distance;
        int16_t angle;
    } s;
    uint8_t u8[6];
} create_data_group2_t;

typedef union create_data_group3
{
    struct
    {
        uint8_t charging_state;
        int16_t voltage;
        int16_t current;
        int8_t battery_temperature;
        uint16_t battery_charge;
        uint16_t battery_capacity;
    } s;
    uint8_t u8[10];
} create_data_group3_t;

typedef union create_data_group4
{
    struct
    {
        uint16_t wall_signal;
        uint16_t cliff_left_signal;
        uint16_t cliff_front_left_signal;
        uint16_t cliff_front_right_signal;
        uint16_t cliff_right_signal;
        union
        {
            struct
            {
                uint8_t digital_input_0 : 1;
                uint8_t digital_input_1 : 1;
                uint8_t digital_input_2 : 1;
                uint8_t digital_input_3 : 1;
                uint8_t baudrate_change : 1;
                uint8_t _reserved : 3;
            } s;
            uint8_t u8;
        } user_digital_inputs;
        uint16_t user_analog_input;
        union
        {
            struct
            {
                uint8_t internal_charger : 1;
                uint8_t home_base : 1;
                uint8_t _reserved : 6;
            } s;
            uint8_t u8;
        } charging_sources_available;
    } s;
    uint8_t u8[14];
} create_data_group4_t;

typedef union create_data_group5
{
    struct
    {
        uint8_t oi_mode;
        uint8_t song_number;
        uint8_t song_playing;
        uint8_t number_of_stream_packets;
        int16_t velocity;
        int16_t radius;
        int16_t right_velocity;
        int16_t left_velocity;
    } s;
    uint8_t u8[12];
} create_data_group5_t;

typedef union create_data
{
    struct
    {
        // group1: 10 bytes, packets 7 - 16
        create_data_group1_t g1;

        // group2: 6 bytes, packets 17 - 20
        create_data_group2_t g2;

        // group3: 10 bytes, packets 21 - 26
        create_data_group3_t g3;
        
        // group4: 14 bytes, packets 27 - 34
        create_data_group4_t g4;
        
        // group5: 12 bytes, packets 35 - 42
        create_data_group5_t g5;
    } s;
    uint8_t u8[52];
} create_data_t;

typedef enum create_packet_group1
{
    create_packet_group1_bumps_and_wheel_drops = 0,
    create_packet_group1_wall,
    create_packet_group1_cliff_left,
    create_packet_group1_cliff_front_left,
    create_packet_group1_cliff_front_right,
    create_packet_group1_cliff_right,
    create_packet_group1_virtual_wall,
    create_packet_group1_overcurrents,
    create_packet_group1_count
} create_packet_group1_t;

typedef enum create_packet_group2
{    
    create_packet_group2_ir_byte = 0,
    create_packet_group2_buttons,
    create_packet_group2_distance,
    create_packet_group2_angle,
    create_packet_group2_count
} create_packet_group2_t;

typedef enum create_packet_group3
{
    create_packet_group3_charging_state = 0,
    create_packet_group3_voltage,
    create_packet_group3_current,
    create_packet_group3_battery_temperature,
    create_packet_group3_battery_charge,
    create_packet_group3_battery_capacity,
    create_packet_group3_count
} create_packet_group3_t;

typedef enum create_packet_group4
{
    create_packet_group4_wall_signal = 0,
    create_packet_group4_cliff_left_signal,
    create_packet_group4_cliff_front_left_signal,
    create_packet_group4_cliff_front_right_signal,
    create_packet_group4_cliff_right_signal,
    create_packet_group4_user_digital_inputs,
    create_packet_group4_user_analog_input,
    create_packet_group4_charging_sources_available,
    create_packet_group4_count
} create_packet_group4_t;

typedef enum create_packet_group5
{
    create_packet_group5_oi_mode = 0,
    create_packet_group5_song_number,
    create_packet_group5_song_playing,
    create_packet_group5_number_of_stream_packets,
    create_packet_group5_velocity,
    create_packet_group5_radius,
    create_packet_group5_right_velocity,
    create_packet_group5_left_velocity,
    create_packet_group5_count
} create_packet_group5_t;

typedef struct create_packet_mask
{
    uint8_t group1;
    uint8_t group2;
    uint8_t group3;
    uint8_t group4;
    uint8_t group5;
} create_packet_mask_t;

#define CREATE_PACKET_MASK(_group, _name) (1 << create_packet_group##_group##_##_name)
#define CREATE_PACKET_MASK_TEST(_mask, _group, _name) (_mask.group##_group & CREATE_PACKET_MASK(_group, _name))
#define CREATE_PACKET_HEADER 19
#define CREATE_PACKET_ID_MAX 42

typedef union create_led
{
    struct
    {
        union
        {
            struct
            {
                uint8_t _reserved_0 : 1;
                uint8_t play : 1;
                uint8_t _reserved_1 : 1;
                uint8_t advance : 1;
                uint8_t _reserved_2 : 4;
            } s;
            uint8_t u8;
        } advance_and_play;
        uint8_t power_color;
        uint8_t power_intensity;
    } s;
    uint8_t u8[3];
} create_led_t;

typedef void (*create_data_callback_t)(void *, const create_data_t *, const create_packet_mask_t *);

class CreateRobot
{
public:

    CreateRobot();
    
    void setup(uint8_t power_status_pin, uint8_t power_toggle_pin, HardwareSerial *serial = &Serial);
    void ondata(create_data_callback_t data_callback, void *data_context = 0);
    void run();

    void off();
    void passive();
    void safe();
    void full();

    void drive(int16_t speed_left, int16_t speed_right);
    void led(bool advance, bool play, uint8_t power_color, uint8_t power_intensity);

    const create_data_t &get() const;

private:

    void read_packet();
    void process_packet();
    void control_power();
    void control_mode();
    void switch_mode();
    void drive_safety();

    create_mode_t mode;
    create_mode_t mode_requested;
    uint32_t mode_timestamp;

    HardwareSerial *serial;

    create_packet_state_t packet_state;
    uint8_t packet_length;
    uint8_t packet_index;
    uint8_t packet_data[255];
    uint8_t packet_checksum;
    
    create_data_t data;
    uint32_t data_timestamp;
    create_data_callback_t data_callback;
    void *data_context;

    bool power;
    bool power_requested;
    uint8_t power_status_pin;
    uint8_t power_toggle_pin;
    uint32_t power_status_timestamp;
    uint32_t power_toggle_timestamp;
    
    uint32_t drive_timestamp;
};

extern CreateRobot Create;

#endif
