#include <create.h>
#include <timer.h>
#include <xbee.h>
#include <network.h>

void network_receive_callback(
  void *context,
  uint8_t source,
  const network_packet_t *packet
)
{
  if (packet->length < 1)
  {
    return;
  }

  switch(packet->data[0])
  {
    case 'M':
      // mode command
      if (packet->length != 2 || packet->data[1] > 3)
      {
        return;
      }

      switch(packet->data[1])
      {
        case 0:
          Create.off();
          break;
        case 1:
          Create.passive();
          break;
        case 2:
          Create.safe();
          break;
        case 3:
          Create.full();
          break;
      }
      break;

    case 'D':
      // drive command
      if (packet->length != 5)
      {
        return;
      }
      Create.drive((int16_t)packet->data[1] << 8 | (int16_t)packet->data[2],
                   (int16_t)packet->data[3] << 8 | (int16_t)packet->data[4]);
      break;
  }
}

void create_data_callback(
  void *context,
  const create_data_t *data,
  const create_packet_mask_t *mask
)
{
}

void setup()
{
  Serial.begin(115200);

  // setup timer to keep track of time
  // this is required by Network and Create
  Timer.setup();

  // begin serial port to xbee
  Serial2.begin(9600);

  // setup xbee
  Xbee.setup(&Serial2);
  
  // R is our network address
  Network.onreceive(network_receive_callback);
  Network.setup('R');

  Create.setup(25, 24, &Serial1);
  Create.ondata(create_data_callback);

  sei();
}

void loop()
{
  Create.run();

  // grab data off serial to construct packet in buffer.
  // this will trigger callback when the full packet is received.  
  // make sure delay is not too long since the serial buffer
  // can only hold up to 128 bytes, we need to call xbee.run()
  // often enough to guarantee packet reception.
  Xbee.run();
  Network.run();
}
