#include <timer.h>

#define LED 19

volatile uint32_t count = 0;
volatile uint32_t timestamp = 0;
uint32_t total = 0;

void setup()
{
    pinMode(LED, OUTPUT);

    Serial.begin(9600);

    // pin change interrupt on INT0
    // falling edge generates interrupt
    EICRA = (1<<ISC01);
    EIMSK = (1<<INT0);

    Timer.setup();
    sei();

    delay(1000);
}

void loop()
{
    delay(20);
    digitalWrite(LED, HIGH);

    uint32_t t = timestamp + 1;
    uint32_t c = count;
    count = 0;
    
    if (c == 0)
    {
        return;
    }

    total += c;

    Serial.print("timestamp = ");
    Serial.print(Timer.getTimestamp());
    Serial.print(", total = ");
    Serial.print(total);
    Serial.print(", cpm = ");
    Serial.print(total * 60 / t);
    Serial.println("");
}

ISR(INT0_vect)
{		
    count ++;
    timestamp = Timer.getTimestamp();
    digitalWrite(LED, LOW);
}


