#include <timer.h>

void setup()
{
  Serial.begin(9600);
  Timer.setup();
}

void loop()
{
  uint32_t timestamp = Timer.getTimestamp();
  Serial.println(timestamp);
  delay(1000);
}
