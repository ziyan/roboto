#include "timer.h"

HardwareTimer Timer;

HardwareTimer::HardwareTimer() :
    ticks(0),
    timestamp(0),
    milliseconds(0)
{
}

void HardwareTimer::setup()
{
    TCCR2A = (0 << WGM21) | (0 << WGM20);
#if F_CPU == 16000000UL
    TCCR2B = (1 << CS22) | (0 << CS21) | (1 << CS20) | (0 << WGM22);
#else
    TCCR2B = (1 << CS22) | (0 << CS21) | (0 << CS20) | (0 << WGM22);
#endif
    TIMSK2 = (1 << TOIE2);
    ASSR = 0;
    TCNT2 = 6;
}

uint64_t HardwareTimer::getTicks() const
{
    return ticks;
}

void HardwareTimer::setTicks(uint64_t ticks)
{
    this->ticks = ticks;
    this->timestamp = (uint32_t)(this->ticks / 1000);
    this->milliseconds = this->ticks % 1000;
}

void HardwareTimer::tick()
{
    ticks += 2;
    milliseconds += 2;

    if (milliseconds >= 1000)
    {
        milliseconds -= 1000;
        ++timestamp;
    }
}

uint32_t HardwareTimer::getTimestamp() const
{
    return timestamp;
}

ISR(TIMER2_OVF_vect)
{
    TCNT2 = 6;
    Timer.tick();
}

