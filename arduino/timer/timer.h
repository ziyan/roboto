#ifndef _TIMER_H_
#define _TIMER_H_

#include <Arduino.h>

class HardwareTimer
{
public:
    HardwareTimer();

    void setup();

    uint64_t getTicks() const;
    void setTicks(uint64_t);
    void tick();

    uint32_t getTimestamp() const;

private:
    volatile uint64_t ticks;
    volatile uint32_t timestamp;
    volatile uint32_t milliseconds;
};

extern HardwareTimer Timer;

#endif // _TIMER_H_

