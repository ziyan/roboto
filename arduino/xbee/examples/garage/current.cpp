#include "current.h"
#include <timer.h>

CurrentSensor::CurrentSensor() :
    port(0),
    timestamp(0),
    sample_interval(0),
    sample_size(0),
    sample_value(0),
    sample_count(0),
    batch(0),
    data(0),
    length(0),
    callback(0),
    context(0)
{
}

void CurrentSensor::setup(uint8_t port, current_sensor_callback_t callback, void *context, uint32_t sample_interval, uint32_t sample_size, uint8_t batch)
{
    this->port = port;
    this->callback = callback;
    this->context = context;
    this->sample_interval = sample_interval;
    this->sample_size = sample_size;
    this->batch = batch;
    this->timestamp = 0;
    this->sample_value = 0;
    this->sample_count = 0;
    
    this->data = (uint16_t *)calloc(this->batch, sizeof(uint16_t));
}

void CurrentSensor::run()
{
    if ((uint32_t)(Timer.getTicks() - this->timestamp) < this->sample_interval)
    {
        return;
    }
    
    this->timestamp = Timer.getTicks();

    // sample measurement
    this->sample_value += get();
    ++this->sample_count;

    if (this->sample_count < this->sample_size)
    {
        return;
    }

    // gather enough sample to do average
    this->data[this->length] = (uint16_t)(this->sample_value / this->sample_count);
    ++this->length;

    this->sample_count = 0;
    this->sample_value = 0;
    
    if (this->length < batch)
    {
        return;
    }

    // gather enough measurement and do callback
    if (this->callback)
    {
        this->callback(this->context, this->timestamp, this->data, this->length);
    }
    this->length = 0;
}

uint16_t CurrentSensor::get() const
{
    return analogRead(port);
}
