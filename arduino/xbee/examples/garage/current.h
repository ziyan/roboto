#ifndef _CURRENT_H_
#define _CURRENT_H_

#include <Arduino.h>

typedef void (*current_sensor_callback_t)(void *, uint64_t timestamp, uint16_t *data, uint8_t length);

class CurrentSensor
{
public:
    CurrentSensor();

    void setup(uint8_t port, current_sensor_callback_t callback = 0, void *context = 0, uint32_t sample_interval = 25, uint32_t sample_size = 40, uint8_t batch = 7);
    void run();
    uint16_t get() const;

private:
    uint8_t port;
    uint64_t timestamp;
    uint32_t sample_interval;
    uint32_t sample_size;
    uint32_t sample_value;
    uint32_t sample_count;
    uint8_t batch;
    uint16_t *data;
    uint8_t length;
    current_sensor_callback_t callback;
    void *context;
};

#endif

