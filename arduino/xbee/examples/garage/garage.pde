#include <timer.h>
#include <xbee.h>
#include <network.h>
#include "pump.h"
#include "current.h"

Pump pumps[4];
CurrentSensor current_sensors[2];

uint8_t network_packet_buffer[64] = {};
network_packet_t network_packet = {network_packet_buffer, 0};

#define LED_PIN 13
bool led_state = false;
void led_toggle()
{
  if (led_state)
  {
    digitalWrite(LED_PIN, LOW);
  }
  else
  {
    digitalWrite(LED_PIN, HIGH);
  }
  led_state = !led_state;
}

void network_receive_callback(void *context, uint8_t procotol, uint8_t destination, uint8_t source, const network_packet_t *packet)
{
  led_toggle();

  if (packet->length < 1)
  {
    return;
  }

  switch(packet->data[0])
  {
    case 'P':
      // pump command
      if (packet->length != 3 || packet->data[1] > 3)
      {
        return;
      }
      if (packet->data[2])
      {
        pumps[packet->data[1]].on();
      }
      else
      {
        pumps[packet->data[1]].off();
      }
      break;

    case 'p':
      // pump status query
      if (packet->length != 2 || packet->data[1] > 3)
      {
        return;
      }
      network_packet.data[0] = 'p';
      network_packet.data[1] = packet->data[1];
      network_packet.data[2] = pumps[packet->data[1]].get();
      network_packet.length = 3;
      Network.unicast(NETWORK_PROTOCOL_DATA, source, &network_packet);
      break;

    case 'c':
      if (packet->length != 2 || packet->data[1] > 1)
      {
        return;
      }
      uint16_t data = current_sensors[packet->data[1]].get();
      network_packet.data[0] = 'c';
      network_packet.data[1] = packet->data[1];
      network_packet.data[2] = (uint8_t)(data & 0xff);
      network_packet.data[3] = (uint8_t)(data >> 8);
      network_packet.length = 4;
      Network.unicast(NETWORK_PROTOCOL_DATA, source, &network_packet);
      break;
  }
}

void pump_callback(void *context, pump_state_t state)
{
  led_toggle();

  network_packet.data[0] = 'p';
  network_packet.data[1] = (uint8_t)(uint16_t)context;
  network_packet.data[2] = state;
  network_packet.length = 3;
  Network.broadcast(NETWORK_PROTOCOL_DATA, &network_packet);
}

void current_sensor_callback(void *context, uint64_t timestamp, uint16_t *data, uint8_t length)
{
  if (!data || length > 20)
  {
    return;
  }

  led_toggle();

  network_packet.data[0] = 'c';
  network_packet.data[1] = (uint8_t)(uint16_t)context;
  memcpy(&network_packet.data[2], &timestamp, sizeof(uint64_t));
  memcpy(&network_packet.data[2 + sizeof(uint64_t)], data, sizeof(uint16_t) * length);
  network_packet.length = 2 + sizeof(uint64_t) + length * sizeof(uint16_t);
  Network.broadcast(NETWORK_PROTOCOL_DATA, &network_packet);
}

void setup()
{
  // led setup
  pinMode(LED_PIN, OUTPUT);
  digitalWrite(LED_PIN, LOW);

  // setup timer to keep track of time
  // this is required by Network and Pump
  Timer.setup();

  // setup pumps
  pumps[0].setup(2, pump_callback, (void *)0);
  pumps[1].setup(3, pump_callback, (void *)1);
  pumps[2].setup(4, pump_callback, (void *)2);
  pumps[3].setup(5, pump_callback, (void *)3);
  
  // setup current sensor
  current_sensors[0].setup(0, current_sensor_callback, (void *)0);
  current_sensors[1].setup(1, current_sensor_callback, (void *)1);

  // begin serial port to xbee
  Serial.begin(9600);
  
  // setup xbee
  Xbee.setup();

  // G is our network address
  Network.setup('G');
  Network.handle(NETWORK_PROTOCOL_DATA, network_receive_callback);

  // enable interrupt
  // required by timer
  sei();
  
  // system functional
  led_toggle();
}

void loop()
{
  // grab data off serial to construct packet in buffer.
  // this will trigger callback when the full packet is received.  
  // make sure delay is not too long since the serial buffer
  // can only hold up to 128 bytes, we need to call xbee.run()
  // often enough to guarantee packet reception.
  Xbee.run();
  Network.run();

  // mamange the pumps
  pumps[0].run();
  pumps[1].run();
  pumps[2].run();
  pumps[3].run();

  // manage current sensors
  current_sensors[0].run();
  current_sensors[1].run();
}
