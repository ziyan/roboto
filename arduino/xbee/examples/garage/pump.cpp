#include "pump.h"
#include <timer.h>

#define PUMP_WARMUP_TIME ((uint32_t)30)
#define PUMP_COOLDOWN_TIME ((uint32_t)30)
#define PUMP_TTL ((uint32_t)300)

Pump::Pump() :
    port(0),
    timestamp(0),
    state(pump_state_off),
    pending(false),
    callback(0),
    context(0)
{
}

void Pump::setup(uint8_t port, pump_callback_t callback, void *context)
{
    this->port = port;
    this->state = pump_state_off;
    this->timestamp = 0;
    this->callback = callback;
    this->context = context;
    pinMode(this->port, OUTPUT);
    digitalWrite(this->port, LOW);
}

void Pump::run()
{
    switch(this->state)
    {
    case pump_state_off:
        break;
    case pump_state_on:
        if ((uint32_t)(Timer.getTimestamp() - this->timestamp) >= PUMP_TTL)
        {
            // no command seen for a while, turn pump off.
            this->state = pump_state_cooldown;
            this->timestamp = Timer.getTimestamp();
            digitalWrite(this->port, LOW);
            // notify state change.
            if (this->callback)
            {
                this->callback(this->context, this->state);
            }
        }
        break;
    case pump_state_warmup:
        if ((uint32_t)(Timer.getTimestamp() - this->timestamp) >= PUMP_WARMUP_TIME)
        {
            // pump warmed up enough
            if (this->pending)
            {
                // turn pump off right away.
                this->state = pump_state_cooldown;
                this->timestamp = Timer.getTimestamp();
                this->pending = false;
                digitalWrite(this->port, LOW);
            }
            else
            {
                // transit into on state.
                this->state = pump_state_on;
                this->timestamp = Timer.getTimestamp();
            }
            // notify state change.
            if (this->callback)
            {
                this->callback(this->context, this->state);
            }
        }
        break;
    case pump_state_cooldown:
        if ((uint32_t)(Timer.getTimestamp() - this->timestamp) >= PUMP_COOLDOWN_TIME)
        {
            // pump cooled down enough
            if (this->pending)
            {
                // turn pump on right away.
                this->state = pump_state_warmup;
                this->timestamp = Timer.getTimestamp();
                this->pending = false;
                digitalWrite(this->port, HIGH);
            }
            else
            {
                // transit into off state.
                this->state = pump_state_off;
                this->timestamp = 0;
            }
            // notify state change.
            if (this->callback)
            {
                this->callback(this->context, this->state);
            }
        }
        break;
    }
}

void Pump::on()
{
    switch(this->state)
    {
    case pump_state_on:
        // note the last command time.
        this->timestamp = Timer.getTimestamp();
        // fall through
    case pump_state_warmup:
        // pump already in running state,
        // assert the pin again to make sure.
        digitalWrite(this->port, HIGH);
        // pending shut off should be cleared.
        this->pending = false;
        break;

    case pump_state_cooldown:
        // make sure pump is off.
        digitalWrite(this->port, LOW);
        // set pending turn on.
        this->pending = true;
        break;

    case pump_state_off:
        // turn pump on right away.
        this->state = pump_state_warmup;
        this->timestamp = Timer.getTimestamp();
        digitalWrite(this->port, HIGH);
        // notify state change.
        if (this->callback)
        {
            this->callback(this->context, this->state);
        }
        break;
    }
}

void Pump::off()
{
    switch(this->state)
    {
    case pump_state_off:
    case pump_state_cooldown:
        // pump already in off state,
        // clear the pin again to make sure.
        digitalWrite(this->port, LOW);
        // pending turn on should be cleared.
        this->pending = false;
        break;

    case pump_state_warmup:
        // make sure pump is on.
        digitalWrite(this->port, HIGH);
        // set pending shut off.
        this->pending = true;
        break;

    case pump_state_on:
        // turn pump off right away.
        this->state = pump_state_cooldown;
        this->timestamp = Timer.getTimestamp();
        digitalWrite(this->port, LOW);
        // notify state change.
        if (this->callback)
        {
            this->callback(this->context, this->state);
        }
        break;
    }
}

pump_state_t Pump::get() const
{
    return this->state;
}

