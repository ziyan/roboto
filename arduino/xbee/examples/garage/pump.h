#ifndef _PUMP_H_
#define _PUMP_H_

#include <Arduino.h>

typedef enum pump_state
{
    pump_state_off = 0,
    pump_state_on,
    pump_state_warmup,
    pump_state_cooldown
} pump_state_t;

typedef void (*pump_callback_t)(void *, pump_state_t);

class Pump
{
public:
    Pump();

    void setup(uint8_t port, pump_callback_t callback = 0, void *context = 0);
    void run();
    void on();
    void off();
    pump_state_t get() const;

private:
    uint8_t port;
    uint32_t timestamp;
    pump_state_t state;
    bool pending;
    pump_callback_t callback;
    void *context;
};

#endif

