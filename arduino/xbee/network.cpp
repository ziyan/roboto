#include "network.h"
#include <timer.h>

void network_heartbeat_handler(void*, uint8_t protocol, uint8_t destination, uint8_t source, const network_packet_t *packet)
{
    uint64_t request = 0;
    uint64_t response = 0;
    uint64_t ticks = 0;

    if (destination == NETWORK_BROADCAST_ADDR)
    {
        return;
    }

    if (packet->length != sizeof(uint64_t) + sizeof(uint64_t))
    {
        return;
    }

    request = *((uint64_t *)packet->data);
    response = *((uint64_t *)packet->data + 1);
    ticks = Timer.getTicks();

    if (ticks < request || ticks > request + 2000)
    {
        // round trip time is too great
        return;
    }

    Timer.setTicks(response + (ticks - request) / 2);
}

XbeeNetwork Network;

XbeeNetwork::XbeeNetwork() :
    xbee(0),
    addr(0),
    heartbeat_interval(0),
    heartbeat_timestamp(0),
    limit(0),
    ttl(0),
    buffer(0),
    head(0),
    size(0)
{
    memset(&packet, 0, sizeof(packet));
    memset(&handlers, 0, sizeof(handlers));
}

void XbeeNetwork::handle(uint8_t protocol, network_handler_t handler)
{
    this->handlers[protocol & NETWORK_PROTOCOL_MASK] = handler;
}

void XbeeNetwork::setup(uint8_t addr, XbeeDevice *xbee, uint32_t heartbeat_interval, uint8_t limit, uint32_t ttl, uint8_t buffer)
{
    this->xbee = xbee;
    this->addr = addr;
    this->heartbeat_interval = heartbeat_interval;
    this->limit = limit;
    this->ttl = ttl;
    this->buffer = buffer;

    this->packet.data = (uint8_t *)calloc(this->buffer, sizeof(uint8_t));
    
    this->xbee->onreceive(XbeeNetwork::xbee_receive_callback, this);

    // listen for heartbeat
    this->handle(NETWORK_PROTOCOL_HEARTBEAT, network_heartbeat_handler);
}

void XbeeNetwork::run()
{
    uint32_t timestamp = Timer.getTimestamp();

    if ((uint32_t)(timestamp - this->heartbeat_timestamp) > heartbeat_interval)
    {
        this->heartbeat();
        this->heartbeat_timestamp = timestamp;
    }
}

void XbeeNetwork::heartbeat()
{
    uint64_t timestamp = Timer.getTicks();
    network_packet_t packet = {(uint8_t *)&timestamp, sizeof timestamp};

    // emit heartbeat with device time
    this->broadcast(NETWORK_PROTOCOL_HEARTBEAT, &packet);
}

void XbeeNetwork::xbee_receive_callback(void *context, const xbee_packet_t *packet, uint8_t flags)
{
    XbeeNetwork *network = (XbeeNetwork *)context;
    network->received(packet);
}

void XbeeNetwork::received(const xbee_packet_t *packet)
{
    uint8_t version = 0;
    uint8_t protocol = 0;
    uint8_t destination = 0;
    uint8_t source = 0;

    if (packet->length < 3)
    {
        return;
    }

    version = packet->data[0] & NETWORK_VERSION_MASK;
    protocol = packet->data[0] & NETWORK_PROTOCOL_MASK;
    destination = packet->data[1];
    source = packet->data[2];

    if (version != NETWORK_VERSION)
    {
        return;
    }

    if (!destination || !source || source == NETWORK_BROADCAST_ADDR)
    {
        return;
    }
    
    record(packet->data[2], &packet->addr64, &packet->addr16);

    if (destination != this->addr && destination != NETWORK_BROADCAST_ADDR)
    {
        return;
    }

    network_packet_t network_packet = {};
    network_packet.data = packet->length > 3 ? &packet->data[3] : 0;
    network_packet.length = packet->length - 3;

    if (this->handlers[protocol])
    {
        this->handlers[protocol](this, protocol, destination, source, &network_packet);
    }
}

void XbeeNetwork::broadcast(uint8_t protocol, const network_packet_t *packet)
{
    this->transmit(protocol, NETWORK_BROADCAST_ADDR, this->addr, packet);
}

void XbeeNetwork::unicast(uint8_t protocol, uint8_t destination, const network_packet_t *packet)
{
    this->transmit(protocol, destination, this->addr, packet);
}

void XbeeNetwork::transmit(uint8_t protocol, uint8_t destination, uint8_t source, const network_packet_t *packet)
{
    if (!destination || destination == this->addr)
    {
        return;
    }

    if (packet && packet->length + 3 > this->buffer)
    {
        return;
    }
    
    resolve(destination, &this->packet.addr64, &this->packet.addr16);

    this->packet.data[0] = NETWORK_VERSION | (protocol & NETWORK_PROTOCOL_MASK);
    this->packet.data[1] = destination;
    this->packet.data[2] = source;
    this->packet.length = 3;

    if (packet && packet->length != 0)
    {
        memcpy(&this->packet.data[3], packet->data, packet->length);
        this->packet.length += packet->length;
    }

    this->xbee->transmit(&this->packet);
}

void XbeeNetwork::record(uint8_t addr, const xbee_addr64 *addr64, const xbee_addr16 *addr16)
{
    network_address_t *address = 0;
    network_address_t *previous = 0;

    if (!addr || addr == NETWORK_BROADCAST_ADDR || addr == this->addr)
    {
        return;
    }

    // find existing record    
    for (address = this->head, previous = 0;
         address != 0;
         previous = address, address = address->next)
    {
        if (address->addr != addr)
        {
            continue;
        }

        address->addr64 = *addr64;
        address->addr16 = *addr16;
        address->timestamp = Timer.getTimestamp();

        // move to head
        if (previous)
        {
            previous->next = address->next;
            address->next = this->head;
            this->head = address;
        }

        return;
    }

    // allocate new record and add to head
    if (size < limit)
    {
        address = (network_address_t *)malloc(sizeof(network_address_t));
        if (address)
        {
            address->addr = addr;
            address->addr64 = *addr64;
            address->addr16 = *addr16;
            address->timestamp = Timer.getTimestamp();
            address->next = this->head;
            this->head = address;
            ++this->size;
            return;
        }
    }
    
    // if allocation failed or limit reached
    // reuse the record at the tail
    // find tail first
    for(address = this->head, previous = 0;
        address != 0 && address->next != 0;
        previous = address, address = address->next)
    {
    }

    // no record exists
    // cannot reuse
    if (!address)
    {
        return;
    }

    address->addr = addr;
    address->addr64 = *addr64;
    address->addr16 = *addr16;
    address->timestamp = Timer.getTimestamp();

    // replaced the only record    
    if (!previous)
    {        
        return;
    }

    // move to head
    previous->next = address->next;
    address->next = this->head;
    this->head = address;
}

void XbeeNetwork::resolve(uint8_t addr, xbee_addr64 *addr64, xbee_addr16 *addr16)
{
    network_address_t *address = 0;
    network_address_t *previous = 0;

    if (!addr || addr == NETWORK_BROADCAST_ADDR)
    {
        *addr64 = xbee_broadcast_addr64;
        *addr16 = xbee_broadcast_addr16;
        return;
    }

    // find existing record
    for (address = this->head, previous = 0;
         address != 0;
         previous = address, address = address->next)
    {
        if (address->addr != addr)
        {
            continue;
        }

        if ((uint32_t)(Timer.getTimestamp() - address->timestamp) <= ttl)
        {
            *addr64 = address->addr64;
            *addr16 = address->addr16;
            return;
        }

        // entry exists but expired
        // delete to speed up future resolve
        if (previous)
        {
            previous->next = address->next;
        }
        else
        {
            this->head = address->next;
        }
        address->next = 0;
        --this->size;

        free(address);
        address = 0;
        break;
    }

    // record not found or expired
    *addr64 = xbee_broadcast_addr64;
    *addr16 = xbee_broadcast_addr16;
}

