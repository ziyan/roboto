#ifndef _NETWORK_H_
#define _NETWORK_H_

#include "xbee.h"

#define NETWORK_VERSION ((uint8_t)(0x80))
#define NETWORK_VERSION_MASK ((uint8_t)(0xf0))

#define NETWORK_PROTOCOL_DATA ((uint8_t)(0x00))
#define NETWORK_PROTOCOL_HEARTBEAT ((uint8_t)(0x0f))
#define NETWORK_PROTOCOL_MASK ((uint8_t)(0x0f))

#define NETWORK_BROADCAST_ADDR ((uint8_t)(0xff))

typedef struct network_packet {
    uint8_t *data;
    uint8_t length;
} network_packet_t;

typedef struct network_address {
    struct network_address *next;
    uint32_t timestamp;
    xbee_addr64 addr64;
    xbee_addr16 addr16;
    uint8_t addr;
} network_address_t;

typedef void (*network_handler_t)(void *, uint8_t protocol, uint8_t destination, uint8_t source, const network_packet_t *);

class XbeeNetwork
{
public:
    XbeeNetwork();

    void handle(uint8_t protocol, network_handler_t handler);
    void setup(uint8_t addr, XbeeDevice *xbee = &Xbee, uint32_t heartbeat_interval = 59, uint8_t limit = 16, uint32_t ttl = 300, uint8_t buffer = 128);
    void run();
    void heartbeat();
    void transmit(uint8_t protocol, uint8_t destination, uint8_t source, const network_packet_t *packet);
    void unicast(uint8_t protocol, uint8_t destination, const network_packet_t *packet);
    void broadcast(uint8_t protocol, const network_packet_t *packet);

private:
    static void xbee_receive_callback(void *context, const xbee_packet_t *packet, uint8_t flags);
    void received(const xbee_packet_t *packet);
    void record(uint8_t addr, const xbee_addr64 *addr64, const xbee_addr16 *addr16);
    void resolve(uint8_t addr, xbee_addr64 *addr64, xbee_addr16 *addr16);

    XbeeDevice *xbee;
    uint8_t addr;
    uint32_t heartbeat_interval;
    uint32_t heartbeat_timestamp;
    uint8_t limit;
    uint32_t ttl;
    uint8_t buffer;
    xbee_packet_t packet;
    network_address_t *head;
    uint8_t size;
    network_handler_t handlers[16];
};

extern XbeeNetwork Network;

#endif

