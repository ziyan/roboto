#include "xbee.h"

XbeeDevice Xbee;

XbeeDevice::XbeeDevice() :
    serial(0),
    receive_callback(0),
    receive_context(0),
    buffer(0),
    index(0),
    state(xbee_state_stx)
{
    memset(&frame, 0, sizeof(frame));
    memset(&packet, 0, sizeof(packet));
}

void XbeeDevice::onreceive(xbee_receive_callback_t receive_callback, void *receive_context)
{
    this->receive_callback = receive_callback;
    this->receive_context = receive_context;
}

void XbeeDevice::setup(HardwareSerial *serial, uint16_t buffer)
{
    this->serial = serial;
    this->buffer = buffer;

    frame.data = (uint8_t *)calloc(this->buffer, sizeof(uint8_t));
}

void XbeeDevice::run()
{
    read();
}

void XbeeDevice::read()
{
    int data = 0;
    uint8_t byte = 0;

    for (;;)
    {
        data = serial->read();
        if (data < 0)
        {
            break;
        }
        byte = (uint8_t)(data & 0xffu);

        switch (state)
        {
        case xbee_state_stx:
            if (byte == XBEE_STX)
            {
                state = xbee_state_length_msb;
            }
            break;

        case xbee_state_length_msb:
            frame.length = (uint16_t)(byte << 8);
            state = xbee_state_length_lsb;
            break;

        case xbee_state_length_lsb:
            frame.length |= (uint16_t)(byte & 0x00ffu);
            if (!frame.length)
            {
                // invalid packet with zero length body
                state = xbee_state_stx;
            }
            else
            {
                state = xbee_state_type;
            }
            break;

        case xbee_state_type:
            frame.checksum = byte;
            frame.type = byte;
            frame.length--;
            if (!frame.length)
            {
                // frame with only type
                state = xbee_state_checksum;
            }
            else
            {
                state = xbee_state_data;
                index = 0;
            }
            break;
            
        case xbee_state_data:
            frame.checksum += byte;
            
            // if we cannot handle this frame, just consume the data silently.
            if (frame.length <= buffer)
            {
                frame.data[index] = byte;
                
            }

            index++;

            if (index >= frame.length)
            {
                // we've got enough data now, continue to check checksum
                state = xbee_state_checksum;
            }
            break;

        case xbee_state_checksum:
            frame.checksum += byte;
            if (frame.checksum == 0xffu)
            {
                frame.checksum = byte;

                // checksum correct
                process();
            }
            state = xbee_state_stx;
            break;

        default:
            state = xbee_state_stx;
            break;
        }
    }
}

void XbeeDevice::process()
{
    uint8_t i = 0;
    switch(frame.type)
    {
        case XBEE_ZIGBEE_RECEIVE_PACKET_FRAME:

            if (frame.length < 11 || frame.length - 11 > XBEE_MAX_PACKET_LENGTH)
            {
                // frame too small or too large to be real packet
                break;
            }

            // convert frame to packet.
            for (i = 0; i < 8; i++)
            {
                packet.addr64.u8[7 - i] = frame.data[i];
            }
            packet.addr16.u8[1] = frame.data[8];
            packet.addr16.u8[0] = frame.data[9];
            packet.data = &frame.data[11];
            packet.length = frame.length - 11;

            if (receive_callback)
            {
                receive_callback(receive_context, &packet, frame.data[10]);
            }
            break;
    }
}

void XbeeDevice::transmit(const xbee_packet_t *packet, uint8_t id, uint8_t hops, uint8_t options)
{
    uint16_t length = 0;
    uint8_t checksum = 0;
    uint8_t i = 0;

    if (packet->length > XBEE_MAX_PACKET_LENGTH || hops > XBEE_MAX_HOPS || (options & ~0x08) != 0)
    {
        return;
    }

    frame.type = XBEE_ZIGBEE_TRANSMIT_REQUEST_FRAME;
    length = 1 + 1 + 8 + 2 + 1 + 1 + packet->length;

    serial->write(XBEE_STX);

    serial->write((uint8_t)(length >> 8));
    serial->write((uint8_t)(length & 0xffu));

    serial->write(XBEE_ZIGBEE_TRANSMIT_REQUEST_FRAME); checksum += XBEE_ZIGBEE_TRANSMIT_REQUEST_FRAME;

    serial->write(id); checksum += id;

    for (i = 0; i < 8; i++)
    {
        serial->write(packet->addr64.u8[7 - i]); checksum += packet->addr64.u8[7 - i];
    }

    serial->write(packet->addr16.u8[1]); checksum += packet->addr16.u8[1];
    serial->write(packet->addr16.u8[0]); checksum += packet->addr16.u8[0];
    
    serial->write(hops); checksum += hops;

    serial->write(options); checksum += options;

    for (i = 0; i < packet->length; i++)
    {
        serial->write(packet->data[i]); checksum += packet->data[i];
    }
    
    checksum = 0xff - checksum;
    serial->write(checksum);
}
