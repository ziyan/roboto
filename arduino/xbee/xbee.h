#ifndef _XBEE_H_
#define _XBEE_H_

#include <Arduino.h>

#define XBEE_STX 0x7e
#define XBEE_MAX_PACKET_LENGTH 72
#define XBEE_MAX_HOPS 10

#define XBEE_MODEM_STATUS_FRAME 0x8A
#define XBEE_AT_COMMAND_FRAME 0x08
#define XBEE_AT_COMMAND_QUERY_PARAMETER_VALUE_FRAME 0x09
#define XBEE_AT_COMMAND_RESPONSE_FRAME 0x88
#define XBEE_REMOTE_COMMAND_REQUEST_FRAME 0x17
#define XBEE_REMOTE_COMMAND_RESPONSE_FRAME 0x97
#define XBEE_ZIGBEE_TRANSMIT_REQUEST_FRAME 0x10
#define XBEE_EXPLICIT_ADDRESSING_ZIGBEE_COMMAND_FRAME 0x11
#define XBEE_ZIGBEE_TRANSMIT_STATUS_FRAME 0x8B
#define XBEE_ZIGBEE_RECEIVE_PACKET_FRAME 0x90
#define XBEE_ZIGBEE_EXPLICIT_RX_INDICATOR_FRAME 0x91
#define XBEE_ZIGBEE_IO_DATA_SAMPLE_RX_INDICATOR_FRAME 0x92
#define XBEE_SENSOR_READ_INDICATOR_FRAME 0x94
#define XBEE_NODE_IDENTIFICATION_INDICATOR_FRAME 0x95

typedef union xbee_addr64 {
    uint64_t u64;
    uint8_t u8[8];
} xbee_addr64_t;

typedef union xbee_addr16 {
    uint16_t u16;
    uint8_t u8[2];
} xbee_addr16_t;

const xbee_addr64 xbee_broadcast_addr64 = ((xbee_addr64_t){0x000000000000ffffu});
const xbee_addr16 xbee_broadcast_addr16 ((xbee_addr16_t){0xfffeu});

typedef struct xbee_frame {
    uint8_t type;
    uint8_t *data;
    uint16_t length;
    uint8_t checksum;
} xbee_frame_t;

typedef struct xbee_packet {
    xbee_addr64_t addr64;
    xbee_addr16_t addr16;
    uint8_t *data;
    uint8_t length;
} xbee_packet_t;

typedef void (*xbee_receive_callback_t)(void *, const xbee_packet_t *, uint8_t);

typedef enum xbee_state
{
    xbee_state_stx = 0,
    xbee_state_length_msb,
    xbee_state_length_lsb,
    xbee_state_type,
    xbee_state_data,
    xbee_state_checksum,
    xbee_state_count
} xbee_state_t;

class XbeeDevice
{
public:
    XbeeDevice();

    void onreceive(xbee_receive_callback_t receive_callback, void *receive_context = 0);
    void setup(HardwareSerial *serial = &Serial, uint16_t buffer = 256);
    void transmit(const xbee_packet_t *packet, uint8_t id = 0, uint8_t hops = 0, uint8_t options = 0);
    void run();

private:
    void read();
    void process();

    HardwareSerial *serial;

    xbee_receive_callback_t receive_callback;
    void *receive_context;

    uint16_t buffer;
    uint16_t index;

    xbee_state_t state;
    xbee_frame_t frame;
    xbee_packet_t packet;
};

extern XbeeDevice Xbee;

#endif
