#ifndef _RBT_XBEE_H_
#define _RBT_XBEE_H_

#include <stdint.h>
#include <termios.h>
#include <pthread.h>

#define RBT_XBEE_MODEM_STATUS_FRAME 0x8A
#define RBT_XBEE_AT_COMMAND_FRAME 0x08
#define RBT_XBEE_AT_COMMAND_QUERY_PARAMETER_VALUE_FRAME 0x09
#define RBT_XBEE_AT_COMMAND_RESPONSE_FRAME 0x88
#define RBT_XBEE_REMOTE_COMMAND_REQUEST_FRAME 0x17
#define RBT_XBEE_REMOTE_COMMAND_RESPONSE_FRAME 0x97
#define RBT_XBEE_ZIGBEE_TRANSMIT_REQUEST_FRAME 0x10
#define RBT_XBEE_EXPLICIT_ADDRESSING_ZIGBEE_COMMAND_FRAME 0x11
#define RBT_XBEE_ZIGBEE_TRANSMIT_STATUS_FRAME 0x8B
#define RBT_XBEE_ZIGBEE_RECEIVE_PACKET_FRAME 0x90
#define RBT_XBEE_ZIGBEE_EXPLICIT_RX_INDICATOR_FRAME 0x91
#define RBT_XBEE_ZIGBEE_IO_DATA_SAMPLE_RX_INDICATOR_FRAME 0x92
#define RBT_XBEE_SENSOR_READ_INDICATOR_FRAME 0x94
#define RBT_XBEE_NODE_IDENTIFICATION_INDICATOR_FRAME 0x95

#define RBT_XBEE_START_DELIMITER 0x7E
#define RBT_XBEE_MAX_PACKET_LENGTH 72
#define RBT_XBEE_MAX_HOPS 10

#ifdef __cplusplus
extern "C" {
#endif

// ********************************************
// device management

typedef struct _rbt_xbee_device
{
    int fd;
    uint8_t *receive_buffer;
    uint32_t receive_buffer_length;
    uint32_t receive_buffer_size;
    uint8_t *transmit_buffer;
    uint32_t transmit_buffer_length;
    uint32_t transmit_buffer_size;
} rbt_xbee_device_t;

#define RBT_XBEE_DEVICE_INITIALIZER ((rbt_xbee_device_t){-1, 0, 0, 0, 0, 0, 0})

int
rbt_xbee_open_device(
    const char* port,
    const speed_t baudrate,
    rbt_xbee_device_t *device
);

void
rbt_xbee_close_device(
    rbt_xbee_device_t *device
);

// ********************************************
// frame operations

typedef struct _rbt_xbee_frame
{
    uint8_t type;
    uint8_t *data;
    uint16_t length;
} rbt_xbee_frame_t;

int
rbt_xbee_receive_frame(
    rbt_xbee_device_t *device,
    rbt_xbee_frame_t *frame
);

int
rbt_xbee_transmit_frame(
    rbt_xbee_device_t *device,
    const rbt_xbee_frame_t *frame
);

int
rbt_xbee_read_frame(
    const uint8_t *buffer,
    uint32_t length,
    uint32_t *size,
    rbt_xbee_frame_t *frame
);

int
rbt_xbee_write_frame(
    uint8_t *buffer,
    uint32_t length,
    uint32_t *size,
    const rbt_xbee_frame_t *frame
);

void
rbt_xbee_free_frame(
    rbt_xbee_frame_t *frame
);

// ********************************************
// packet

typedef union _rbt_xbee_addr64 {
    uint64_t u64;
    uint8_t u8[8];
} rbt_xbee_addr64_t;

typedef union _rbt_xbee_addr16 {
    uint16_t u16;
    uint8_t u8[2];
} rbt_xbee_addr16_t;

#define RBT_XBEE_BROADCAST_ADDR64 ((rbt_xbee_addr64_t){0x000000000000ffffu})
#define RBT_XBEE_BROADCAST_ADDR16 ((rbt_xbee_addr16_t){0xfffeu})


typedef struct _rbt_xbee_packet
{
    rbt_xbee_addr64_t addr64;
    rbt_xbee_addr16_t addr16;
    uint8_t *data;
    uint8_t length;
} rbt_xbee_packet_t;

void
rbt_xbee_free_packet(
    rbt_xbee_packet_t *packet
);

int
rbt_xbee_encode_packet(
    const rbt_xbee_packet_t *packet,
    uint8_t id,
    uint8_t hops,
    uint8_t options,
    rbt_xbee_frame_t *frame
);

int
rbt_xbee_decode_packet(
    const rbt_xbee_frame_t *frame,
    rbt_xbee_packet_t *packet,
    uint8_t *flags
);

int
rbt_xbee_decode_transmit_status(
    const rbt_xbee_frame_t *frame,
    uint8_t *id,
    rbt_xbee_addr16_t *addr16,
    uint8_t *retries,
    uint8_t *delivery_status,
    uint8_t *discovery_status
);

// ********************************************
// xbee queue

typedef struct _rbt_xbee_frame_queue
{
    pthread_mutex_t mutex;
    pthread_cond_t cond;

    rbt_xbee_frame_t *queue;
    rbt_xbee_frame_t *head;
    rbt_xbee_frame_t *tail;

    uint32_t length;
    uint32_t size;
} rbt_xbee_frame_queue_t;

int
rbt_xbee_create_frame_queue(
    uint32_t size,
    rbt_xbee_frame_queue_t *queue
);

void
rbt_xbee_free_frame_queue(
    rbt_xbee_frame_queue_t *queue
);

int
rbt_xbee_enqueue_frame(
    rbt_xbee_frame_queue_t *queue,
    rbt_xbee_frame_t *frame
);

int
rbt_xbee_dequeue_frame(
    rbt_xbee_frame_queue_t *queue,
    rbt_xbee_frame_t *frame,
    uint32_t timeout
);

// ********************************************
// xbee

typedef void (*rbt_xbee_receive_callback_t)(void *, const rbt_xbee_packet_t *, uint8_t);
typedef void (*rbt_xbee_transmit_callback_t)(void *, uint8_t, rbt_xbee_addr16_t, uint8_t, uint8_t, uint8_t);

typedef enum _rbt_xbee_state
{
    rbt_xbee_state_uninitialized = 0,
    rbt_xbee_state_running,
    rbt_xbee_state_terminated
} rbt_xbee_state_t;

typedef struct _rbt_xbee
{
    volatile rbt_xbee_state_t state;

    rbt_xbee_device_t device;

    pthread_t frame_transmit_thread;
    pthread_t frame_receive_thread;

    pthread_t *dispatch_threads;
    uint32_t dispatch_threads_count;

    rbt_xbee_frame_queue_t frame_transmit_queue;
    rbt_xbee_frame_queue_t frame_receive_queue;

    pthread_mutex_t mutex;

    rbt_xbee_receive_callback_t receive_callback;
    void *receive_callback_context;

    rbt_xbee_transmit_callback_t transmit_callback[256];
    void *transmit_callback_context[256];
    uint8_t transmit_callback_id;
} rbt_xbee_t;

int
rbt_xbee_start(
    const char* port,
    speed_t baudrate,
    uint32_t dispatch_threads_count,
    rbt_xbee_t *xbee
);

void
rbt_xbee_stop(
    rbt_xbee_t *xbee
);

int
rbt_xbee_set_receive_callback(
    rbt_xbee_t *xbee,
    rbt_xbee_receive_callback_t receive_callback,
    void *receive_callback_context
);

int
rbt_xbee_transmit(
    rbt_xbee_t *xbee,
    const rbt_xbee_packet_t *packet,
    uint8_t hops,
    uint8_t options,
    rbt_xbee_transmit_callback_t callback,
    void *context,
    uint8_t *id
);

#ifdef __cplusplus
}
#endif

#endif

