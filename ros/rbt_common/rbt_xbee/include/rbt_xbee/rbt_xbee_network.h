#ifndef _RBT_XBEE_NETWORK_H_
#define _RBT_XBEE_NETWORK_H_

#include <stdint.h>
#include <pthread.h>

#include "rbt_xbee.h"

#ifdef __cplusplus
extern "C" {
#endif

#define RBT_XBEE_NETWORK_VERSION ((uint8_t)0x80u)
#define RBT_XBEE_NETWORK_VERSION_MASK ((uint8_t)0xf0u)

#define RBT_XBEE_NETWORK_PROTOCOL_DATA ((uint8_t)0x00u)
#define RBT_XBEE_NETWORK_PROTOCOL_HEARTBEAT ((uint8_t)0x0fu)
#define RBT_XBEE_NETWORK_PROTOCOL_MASK ((uint8_t)0x0fu)

#define RBT_XBEE_NETWORK_BROADCAST_ADDR ((uint8_t)0xffu)

#define RBT_XBEE_NETWORK_PACKET_MAX_LENGTH 69
#define RBT_XBEE_NETWORK_NODE_TTL (5 * 60)
#define RBT_XBEE_NETWORK_HEARTBEAT 60

typedef void (*rbt_xbee_network_protocol_handler_t)(void *, uint8_t, uint8_t, uint8_t, const uint8_t *, uint8_t);

typedef struct _rbt_xbee_network_node
{
    rbt_xbee_addr64_t addr64;
    rbt_xbee_addr16_t addr16;
    time_t timestamp;
} rbt_xbee_network_node_t;

typedef enum _rbt_xbee_network_state
{
    rbt_xbee_network_state_uninitialized = 0,
    rbt_xbee_network_state_running,
    rbt_xbee_network_state_terminated
} rbt_xbee_network_state_t;

typedef struct _rbt_xbee_network
{
    volatile rbt_xbee_network_state_t state;

    uint8_t addr;

    rbt_xbee_t *xbee;

    pthread_t thread;

    rbt_xbee_network_node_t nodes[256];
    uint8_t nodes_count;

    rbt_xbee_network_protocol_handler_t handlers[16];
    
    pthread_mutex_t mutex;
} rbt_xbee_network_t;

int
rbt_xbee_network_start(
    uint8_t addr,
    rbt_xbee_t *xbee,
    rbt_xbee_network_t *network
);

void
rbt_xbee_network_stop(
    rbt_xbee_network_t *network
);

int
rbt_xbee_network_set_protocol_handler(
    rbt_xbee_network_t *network,
    uint8_t protocol,
    rbt_xbee_network_protocol_handler_t handler
);

int
rbt_xbee_network_transmit(
    rbt_xbee_network_t *network,
    uint8_t protocol,
    uint8_t destination,
    uint8_t source,
    const uint8_t *data,
    uint8_t length
);

int
rbt_xbee_network_unicast(
    rbt_xbee_network_t *network,
    uint8_t protocol,
    uint8_t destination,
    const uint8_t *data,
    uint8_t length
);

int
rbt_xbee_network_broadcast(
    rbt_xbee_network_t *network,
    uint8_t protocol,
    const uint8_t *data,
    uint8_t length
);

#ifdef __cplusplus
}
#endif

#endif

