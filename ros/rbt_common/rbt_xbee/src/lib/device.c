#include "stdafx.h"

int
rbt_xbee_open_device(
    const char* port,
    const speed_t baudrate,
    rbt_xbee_device_t *device
)
{
    const uint32_t default_receive_buffer_size = 1024;
    const uint32_t default_transmit_buffer_size = 1024;

    int error = 0;
    int fd = -1;
    struct termios options;
    uint8_t *receive_buffer = 0;
    uint8_t *transmit_buffer = 0;
    
    memset(&options, 0, sizeof options);
    
    if (device)
    {
        *device = RBT_XBEE_DEVICE_INITIALIZER;
    }
    
    if (!port || !device)
    {
        error = -EINVAL;
        goto quit;
    }
    
    fd = open(port, O_RDWR | O_NOCTTY);
    if(fd < 0)
    {
        error = -errno;
        goto quit;
    }

    if (tcgetattr(fd, &options) < 0)
    {
        error = -errno;
        goto quit;
    }

    options.c_cflag = CLOCAL | CREAD | CS8;
    options.c_iflag = 0;
    options.c_oflag = 0;
    options.c_lflag = 0;

    if (cfsetispeed(&options, baudrate) < 0)
    {
        error = -errno;
        goto quit;
    }

    if (cfsetospeed(&options, baudrate) < 0)
    {
        error = -errno;
        goto quit;
    }

    if (tcsetattr(fd, TCSANOW, &options) < 0)
    {
        error = -errno;
        goto quit;
    }
    
    receive_buffer = (uint8_t *)malloc(default_receive_buffer_size * sizeof(uint8_t));
    if (!receive_buffer)
    {
        error = -errno;
        goto quit;
    }
    
    transmit_buffer = (uint8_t *)malloc(default_transmit_buffer_size * sizeof(uint8_t));
    if (!transmit_buffer)
    {
        error = -errno;
        goto quit;
    }

    // no failure
    device->fd = fd;
    device->receive_buffer = receive_buffer;
    receive_buffer = 0;
    device->receive_buffer_size = default_receive_buffer_size;
    device->transmit_buffer = transmit_buffer;
    transmit_buffer = 0;
    device->transmit_buffer_size = default_transmit_buffer_size;
    error = 0;

quit:

    free(receive_buffer);
    receive_buffer = 0;

    free(transmit_buffer);
    transmit_buffer = 0;

    return error;
}

void
rbt_xbee_close_device(
    rbt_xbee_device_t *device
)
{
    if (!device)
    {
        return;
    }

    if (device->fd >= 0)
    {
        close(device->fd);
    }

    free(device->receive_buffer);
    free(device->transmit_buffer);

    *device = RBT_XBEE_DEVICE_INITIALIZER;
}


