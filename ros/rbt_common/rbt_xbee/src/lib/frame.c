#include "stdafx.h"

int
rbt_xbee_receive_frame(
    rbt_xbee_device_t *device,
    rbt_xbee_frame_t *frame
)
{
    int error = 0;
    uint32_t size_consumed = 0;
    ssize_t size_read = 0;

    if (frame)
    {
        memset(frame, 0, sizeof *frame);
    }

    if (!device ||
        device->fd < 0 ||
        !device->receive_buffer ||
        !device->receive_buffer_size ||
        !frame)
    {
        error = -EINVAL;
        goto quit;
    }

    for (;;)
    {
        if (device->receive_buffer_length)
        {
            // if the length is not zero,
            // we need to make sure the existing buffer does not contain any complete frame frist.
            // otherwise we will waste our time trying to get more data out of the device.
            error = rbt_xbee_read_frame(device->receive_buffer,
                                        device->receive_buffer_length,
                                        &size_consumed,
                                        frame);

            // now shift the buffer
            device->receive_buffer_length -= size_consumed;
            memmove(device->receive_buffer,
                    device->receive_buffer + size_consumed,
                    device->receive_buffer_length);

            if (error < 0)
            {
                // an error has occured
                goto quit;
            }

            if (error == 0)
            {            
                // successfully read a frame from buffer
                goto quit;
            }
        }

        if (device->receive_buffer_length >= device->receive_buffer_size)
        {
            // no space left in receive buffer
            error = -ENOBUFS;
            goto quit;
        }
        
        size_read = read(device->fd,
                         device->receive_buffer + device->receive_buffer_length,
                         device->receive_buffer_size - device->receive_buffer_length);
        if (size_read < 0)
        {
            error = -errno;
            goto quit;
        }

        if (size_read == 0)
        {
            // end of file
            error = -ENODATA;
            goto quit;
        }

        // record how much data is in the buffer
        device->receive_buffer_length += size_read;
        assert(device->receive_buffer_length <= device->receive_buffer_size);
    }

quit:

    return error;
}

int
rbt_xbee_transmit_frame(
    rbt_xbee_device_t *device,
    const rbt_xbee_frame_t *frame
)
{
    int error = 0;
    uint32_t size_produced = 0;
    ssize_t size_written = 0;

    if (!device ||
        device->fd < 0 ||
        !device->transmit_buffer ||
        !device->transmit_buffer_size)
    {
        error = -EINVAL;
        goto quit;
    }

    if (frame)
    {
        error = rbt_xbee_write_frame(device->transmit_buffer + device->transmit_buffer_length,
                                     device->transmit_buffer_size - device->transmit_buffer_length,
                                     &size_produced,
                                     frame);
        if (error < 0)
        {
            goto quit;
        }
    
        assert(size_produced > 0);
        device->transmit_buffer_length += size_produced;
    }

    while (device->transmit_buffer_length > 0)
    {
        // write buffer
        size_written = write(device->fd, device->transmit_buffer, device->transmit_buffer_length);
        if (size_written <= 0)
        {
            error = -errno;
            goto quit;
        }

        device->transmit_buffer_length -= size_written;

        // shift unwritten buffer up
        memmove(device->transmit_buffer,
                device->transmit_buffer + size_written,
                device->transmit_buffer_length);
    }
    
    error = 0;

quit:

    return error;
}

int
rbt_xbee_read_frame(
    const uint8_t *buffer,
    uint32_t length,
    uint32_t *size,
    rbt_xbee_frame_t *frame
)
{
    int error = 0;
    uint32_t position = 0;
    uint8_t frame_type = 0;
    uint16_t frame_length = 0;
    uint8_t *frame_data = 0;
    uint8_t frame_checksum = 0;
    uint32_t i = 0;

    if (frame)
    {
        memset(frame, 0, sizeof *frame);
    }
    
    if (size)
    {
        *size = 0;
    }

    if (!buffer ||
        !length ||
        !size ||
        !frame)
    {
        error = -EINVAL;
        goto quit;
    }

    // find stx
    for (position = 0; position < length; position++)
    {
        if (buffer[position] == RBT_XBEE_START_DELIMITER)
        {
            break;
        }
    }

    if (position == length)
    {
        // did not find stx, but consumed the entire buffer
        error = 1;
        *size = length;
        goto quit;
    }

    // [stx][high][low][type][...][checksum]
    // found stx, see if we have space for type length and checksum
    if (length < position + 5)
    {
        // doesn't have the entire packet
        // consume right up to the stx
        error = 1;
        *size = position;
        goto quit;
    }

    // calculate length
    frame_length = (uint16_t)((uint16_t)buffer[position + 1] << 8 | (uint16_t)buffer[position + 2]) - 1;
    
    // see if we have enough data again
    if (length <= position + frame_length + 5)
    {
        // doesn't have the entire packet
        // consume right up to the stx
        error = 1;
        *size = position;
        goto quit;
    }

    // get type
    frame_type = buffer[position + 3];

    // allocate data
    frame_data = (uint8_t *)malloc(frame_length * sizeof(uint8_t));
    if (!frame_data)
    {
        error = -errno;
        *size = position;
        goto quit;
    }

    // copy data
    memcpy(frame_data, &buffer[position + 4], frame_length);
    
    // calculate checksum
    frame_checksum = frame_type;
    for (i = 0; i < frame_length; i++)
    {
        frame_checksum += frame_data[i];
    }
    frame_checksum += buffer[position + frame_length + 4];
    
    if (frame_checksum != (uint8_t)0xff)
    {
        // checksum failed
        error = 1;
        *size = position + frame_length + 5;
        goto quit;
    }

    // no failure
    frame->type = frame_type;
    frame->data = frame_data;
    frame_data = 0;
    frame->length = frame_length;

    *size = position + 1 + 2 + 1 + frame_length + 1;
    error = 0;

quit:

    free(frame_data);
    frame_data = 0;

    return error;
}

int
rbt_xbee_write_frame(
    uint8_t *buffer,
    uint32_t length,
    uint32_t *size,
    const rbt_xbee_frame_t *frame
)
{
    int error = 0;
    uint32_t frame_size = 0;
    uint8_t frame_checksum = 0;
    uint32_t i = 0;
    
    if (size)
    {
        *size = 0;
    }

    if (!buffer ||
        !length ||
        !size ||
        !frame)
    {
        error = -EINVAL;
        goto quit;
    }

    // calculate length
    frame_size = 1 + 2 + 1 + frame->length + 1;
    if (length < frame_size)
    {
        // signal not enough buffer
        error = -ENOBUFS;
        *size = frame_size;
        goto quit;
    }

    // calculate checksum
    frame_checksum = frame->type;
    for (i = 0; i < frame->length; i++)
    {
        frame_checksum += frame->data[i];
    }
    frame_checksum = 0xffu - frame_checksum;

    // no failure
    buffer[0] = RBT_XBEE_START_DELIMITER;
    buffer[1] = (frame->length + 1) >> 8;
    buffer[2] = (frame->length + 1) & 0x00ffu;
    buffer[3] = frame->type;
    memcpy(&buffer[4], frame->data, frame->length);
    buffer[4 + frame->length] = frame_checksum;
    
    *size = frame_size;
    error = 0;

quit:

    return error;
}

void
rbt_xbee_free_frame(
    rbt_xbee_frame_t *frame
)
{
    if (!frame)
    {
        return;
    }

    free(frame->data);
    memset(frame, 0, sizeof *frame);    
}
