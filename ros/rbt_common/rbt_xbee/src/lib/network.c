#include "stdafx.h"

void
rbt_xbee_network_receive_callback(
    void *context,
    const rbt_xbee_packet_t *packet,
    uint8_t flags
)
{
    rbt_xbee_network_t *network = (rbt_xbee_network_t *)context;
    uint8_t version = 0;
    uint8_t protocol = 0;
    uint8_t destination = 0;
    uint8_t source = 0;
    rbt_xbee_network_protocol_handler_t handler = 0;

    if (!network ||
        network->state != rbt_xbee_network_state_running)
    {
        return;
    }

    if (!packet ||
        packet->length < 3)
    {
        return;
    }

    version = packet->data[0] & RBT_XBEE_NETWORK_VERSION_MASK;
    protocol = packet->data[0] & RBT_XBEE_NETWORK_PROTOCOL_MASK;
    destination = packet->data[1];
    source = packet->data[2];

    if (!source ||
        !destination ||
        source == RBT_XBEE_NETWORK_BROADCAST_ADDR ||
        version != RBT_XBEE_NETWORK_VERSION)
    {
        return;
    }
    
    if (pthread_mutex_lock(&network->mutex))
    {
        return;
    }

    if (!network->nodes[source].timestamp)
    {
        // discovered new node
        ++network->nodes_count;
    }

    network->nodes[source].addr16 = packet->addr16;
    network->nodes[source].addr64 = packet->addr64;
    time(&network->nodes[source].timestamp);

    if (destination == network->addr || destination == RBT_XBEE_NETWORK_BROADCAST_ADDR)
    {
        handler = network->handlers[protocol];
    }

    pthread_mutex_unlock(&network->mutex);

    if (handler)
    {
        handler(network,
                protocol,
                destination,
                source,
                packet->length > 3 ? &packet->data[3] : 0,
                packet->length - 3);
    }
}

void
rbt_xbee_network_transmit_callback(
    void *context,
    uint8_t id,
    rbt_xbee_addr16_t addr16,
    uint8_t retries,
    uint8_t delivery_status,
    uint8_t discovery_status
)
{
    rbt_xbee_network_t *network = (rbt_xbee_network_t *)context;
    uint8_t destination = 0;
    
    if (!network ||
        network->state != rbt_xbee_network_state_running ||
        !id)
    {
        return;
    }
    
    if (addr16.u16 ==  RBT_XBEE_BROADCAST_ADDR16.u16 ||
        !delivery_status)
    {
        return;
    }
    
    // delivery failed
    // clear cached node information

    if (pthread_mutex_lock(&network->mutex))
    {
        return;
    }
    
    for (destination = 1; destination < RBT_XBEE_NETWORK_BROADCAST_ADDR; destination++)
    {
        if (!network->nodes[destination].timestamp)
        {
            continue;
        }
        
        if (network->nodes[destination].addr16.u16 == addr16.u16)
        {
            memset(&network->nodes[destination], 0, sizeof network->nodes[destination]);
            --network->nodes_count;
        }
    }

    pthread_mutex_unlock(&network->mutex);
}

void *
rbt_xbee_network_thread(
    void *arg
)
{
    int error = 0;
    int locked = 0;
    rbt_xbee_network_t *network = (rbt_xbee_network_t *)arg;
    time_t heartbeat_timestamp = 0;
    time_t now = 0;
    uint8_t destination = 0;

    if (!network)
    {
        error = -EINVAL;
        goto quit;
    }
    
    while (network->state == rbt_xbee_network_state_running)
    {
        time(&now);
        if (difftime(now, heartbeat_timestamp) >= RBT_XBEE_NETWORK_HEARTBEAT)
        {
            // send heartbeat
            error = rbt_xbee_network_transmit(network,
                                              RBT_XBEE_NETWORK_PROTOCOL_HEARTBEAT,
                                              RBT_XBEE_NETWORK_BROADCAST_ADDR,
                                              network->addr,
                                              0,
                                              0);
            if (error)
            {
                goto quit;
            }
            time(&heartbeat_timestamp);
        }

        // check for expired nodes
        error = -pthread_mutex_lock(&network->mutex);
        if (error)
        {
            goto quit;
        }
        locked = 1;

        for (destination = 1; destination < RBT_XBEE_NETWORK_BROADCAST_ADDR; destination++)
        {
            if (!network->nodes[destination].timestamp)
            {
                continue;
            }

            if (difftime(now, network->nodes[destination].timestamp) > RBT_XBEE_NETWORK_NODE_TTL)
            {
                memset(&network->nodes[destination], 0, sizeof network->nodes[destination]);
                --network->nodes_count;
            }
        }
        
        error = -pthread_mutex_unlock(&network->mutex);
        if (error)
        {
            goto quit;
        }
        locked = 0;

        usleep(1000000);
    }

quit:

    if (locked)
    {
        pthread_mutex_unlock(&network->mutex);
    }

    return (void *)(size_t)error;
}

int
rbt_xbee_network_start(
    uint8_t addr,
    rbt_xbee_t *xbee,
    rbt_xbee_network_t *network
)
{
    int error = 0;
    
    if (network)
    {
        memset(network, 0, sizeof *network);
    }
    
    if (!addr ||
        addr == RBT_XBEE_NETWORK_BROADCAST_ADDR ||
        !xbee ||
        xbee->state != rbt_xbee_state_running ||
        !network)
    {
        error = -EINVAL;
        goto quit;
    }

    network->state = rbt_xbee_network_state_running;
    network->addr = addr;
    network->xbee = xbee;
    
    // create mutex
    error = -pthread_mutex_init(&network->mutex, 0);
    if (error)
    {
        goto quit;
    }

    // register callback
    error = rbt_xbee_set_receive_callback(xbee, rbt_xbee_network_receive_callback, network);
    if (error)
    {
        goto quit;
    }
    
    // create threads
    error = -pthread_create(&network->thread, 0, rbt_xbee_network_thread, network);
    if (error)
    {
        goto quit;
    }

quit:

    if (error)
    {
        rbt_xbee_network_stop(network);
    }

    return error;
}

void
rbt_xbee_network_stop(
    rbt_xbee_network_t *network
)
{
    if (!network)
    {
        return;
    }

    rbt_xbee_set_receive_callback(network->xbee, 0, 0);

    network->state = rbt_xbee_network_state_terminated;
    
    pthread_join(network->thread, 0);

    pthread_mutex_destroy(&network->mutex);

    memset(network, 0, sizeof *network);
}

int
rbt_xbee_network_set_protocol_handler(
    rbt_xbee_network_t *network,
    uint8_t protocol,
    rbt_xbee_network_protocol_handler_t handler
)
{
    int error = 0;
    int locked = 0;

    if (!network || network->state != rbt_xbee_network_state_running || !handler)
    {
        error = -EINVAL;
        goto quit;
    }

    error = -pthread_mutex_lock(&network->mutex);
    if (error)
    {
        goto quit;
    }
    locked = 1;

    network->handlers[protocol & RBT_XBEE_NETWORK_PROTOCOL_MASK] = handler;

quit:

    if (locked)
    {
        pthread_mutex_unlock(&network->mutex);
    }

    return error;
}

int
rbt_xbee_network_transmit(
    rbt_xbee_network_t *network,
    uint8_t protocol,
    uint8_t destination,
    uint8_t source,
    const uint8_t *data,
    uint8_t length
)
{
    int error = 0;
    int locked = 0;
    rbt_xbee_packet_t packet;
    time_t now = 0;
    uint8_t id = 0;

    memset(&packet, 0, sizeof packet);

    if (!network ||
        network->state != rbt_xbee_network_state_running ||
        !destination ||
        (data && !length) ||
        (!data && length) ||
        length > RBT_XBEE_NETWORK_PACKET_MAX_LENGTH)
    {
        error = -EINVAL;
        goto quit;
    }

    packet.addr64 = RBT_XBEE_BROADCAST_ADDR64;
    packet.addr16 = RBT_XBEE_BROADCAST_ADDR16;

    if (destination != RBT_XBEE_NETWORK_BROADCAST_ADDR)
    {
        error = -pthread_mutex_lock(&network->mutex);
        if (error)
        {
            goto quit;
        }
        locked = 1;

        if (network->nodes[destination].timestamp)
        {
            time(&now);
            if (difftime(now, network->nodes[destination].timestamp) > RBT_XBEE_NETWORK_NODE_TTL)
            {
                // node expired
                memset(&network->nodes[destination], 0, sizeof network->nodes[destination]);
                --network->nodes_count;
            }
            else
            {
                packet.addr64 = network->nodes[destination].addr64;
                packet.addr16 = network->nodes[destination].addr16;
            }
        }

        error = -pthread_mutex_unlock(&network->mutex);
        if (error)
        {
            goto quit;
        }
        locked = 0;
    }
    
    packet.length = length + 3;
    packet.data = (uint8_t *)malloc(packet.length * sizeof(uint8_t));
    if (!packet.data)
    {
        error = -errno;
        goto quit;
    }

    packet.data[0] = RBT_XBEE_NETWORK_VERSION | (protocol & RBT_XBEE_NETWORK_PROTOCOL_MASK);
    packet.data[1] = destination;
    packet.data[2] = source;

    if (length)
    {
        memcpy(&packet.data[3], data, length);
    }

    error = rbt_xbee_transmit(network->xbee,
                              &packet,
                              0,
                              0,
                              rbt_xbee_network_transmit_callback,
                              network,
                              &id);
    if (error)
    {
        goto quit;
    }

quit:

    if (locked)
    {
        pthread_mutex_unlock(&network->mutex);
    }

    rbt_xbee_free_packet(&packet);

    return error;
}

int
rbt_xbee_network_unicast(
    rbt_xbee_network_t *network,
    uint8_t protocol,
    uint8_t destination,
    const uint8_t *data,
    uint8_t length
)
{
    return rbt_xbee_network_transmit(network, protocol, destination, network->addr, data, length);
}

int
rbt_xbee_network_broadcast(
    rbt_xbee_network_t *network,
    uint8_t protocol,
    const uint8_t *data,
    uint8_t length
)
{
    return rbt_xbee_network_transmit(network, protocol, RBT_XBEE_NETWORK_BROADCAST_ADDR, network->addr, data, length);
}

