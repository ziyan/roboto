#include "stdafx.h"

void
rbt_xbee_free_packet(
    rbt_xbee_packet_t *packet
)
{
    if (!packet)
    {
        return;
    }

    free(packet->data);
    
    memset(packet, 0, sizeof *packet);
}

int
rbt_xbee_encode_packet(
    const rbt_xbee_packet_t *packet,
    uint8_t id,
    uint8_t hops,
    uint8_t options,
    rbt_xbee_frame_t *frame
)
{
    int error = 0;
    uint16_t length = 0;

    if (frame)
    {
        memset(frame, 0, sizeof *frame);
    }

    if (!packet ||
        !packet->data ||
        !packet->length ||
        packet->length > RBT_XBEE_MAX_PACKET_LENGTH ||
        hops > RBT_XBEE_MAX_HOPS ||
        !frame)
    {
        error = -EINVAL;
        goto quit;
    }

    // calculate required length
    length = 1 + 8 + 2 + 1 + 1 + packet->length;

    frame->data = (uint8_t *)malloc(length * sizeof(uint8_t));
    if (!frame->data)
    {
        error = -errno;
        goto quit;
    }

    // no failure
    frame->type = RBT_XBEE_ZIGBEE_TRANSMIT_REQUEST_FRAME;
    frame->length = length;
    frame->data[0] = id;
    frame->data[1] = packet->addr64.u8[7];
    frame->data[2] = packet->addr64.u8[6];
    frame->data[3] = packet->addr64.u8[5];
    frame->data[4] = packet->addr64.u8[4];
    frame->data[5] = packet->addr64.u8[3];
    frame->data[6] = packet->addr64.u8[2];
    frame->data[7] = packet->addr64.u8[1];
    frame->data[8] = packet->addr64.u8[0];
    frame->data[9] = packet->addr16.u8[1];
    frame->data[10] = packet->addr16.u8[0];
    frame->data[11] = hops;
    frame->data[12] = options;
    memcpy(&frame->data[13], packet->data, packet->length);

quit:

    return error;
}

int
rbt_xbee_decode_packet(
    const rbt_xbee_frame_t *frame,
    rbt_xbee_packet_t *packet,
    uint8_t *flags
)
{
    int error = 0;
    uint8_t length = 0;

    if (packet)
    {
        memset(packet, 0, sizeof *packet);
    }

    if (flags)
    {
        *flags = 0;
    }

    if (!frame ||
        frame->type != RBT_XBEE_ZIGBEE_RECEIVE_PACKET_FRAME ||
        !frame->data ||
        frame->length <= 8 + 2 + 1 ||
        frame->length > 8 + 2 + 1 + RBT_XBEE_MAX_PACKET_LENGTH ||
        !packet ||
        !flags)
    {
        error = -EINVAL;
        goto quit;
    }

    length = frame->length - 8 - 2 - 1;
    assert(length <= RBT_XBEE_MAX_PACKET_LENGTH);

    packet->data = (uint8_t *)malloc(length * sizeof(uint8_t));
    if (!packet->data)
    {
        error = -errno;
        goto quit;
    }

    // no failure
    packet->addr64.u8[7] = frame->data[0];
    packet->addr64.u8[6] = frame->data[1];
    packet->addr64.u8[5] = frame->data[2];
    packet->addr64.u8[4] = frame->data[3];
    packet->addr64.u8[3] = frame->data[4];
    packet->addr64.u8[2] = frame->data[5];
    packet->addr64.u8[1] = frame->data[6];
    packet->addr64.u8[0] = frame->data[7];
    packet->addr16.u8[1] = frame->data[8];
    packet->addr16.u8[0] = frame->data[9];
    *flags = frame->data[10];
    packet->length = length;
    memcpy(packet->data, &frame->data[11], length);
    
quit:

    return error;
}

int
rbt_xbee_decode_transmit_status(
    const rbt_xbee_frame_t *frame,
    uint8_t *id,
    rbt_xbee_addr16_t *addr16,
    uint8_t *retries,
    uint8_t *delivery_status,
    uint8_t *discovery_status
)
{
    int error = 0;

    if (id)
    {
        *id = 0;
    }

    if (addr16)
    {
        addr16->u16 = 0;
    }

    if (retries)
    {
        *retries = 0;
    }

    if (delivery_status)
    {
        *delivery_status = 0;
    }

    if (discovery_status)
    {
        *discovery_status = 0;
    }

    if (!frame ||
        frame->type != RBT_XBEE_ZIGBEE_TRANSMIT_STATUS_FRAME ||
        frame->length != 1 + 2 + 1 + 1 + 1 ||
        !frame->data ||
        !frame->data[0])
    {
        error = -EINVAL;
        goto quit;
    }

    // no failure
    *id = frame->data[0];
    addr16->u8[1] = frame->data[1];
    addr16->u8[0] = frame->data[2];
    *retries = frame->data[3];
    *delivery_status = frame->data[4];
    *discovery_status = frame->data[5];

quit:

    return error;
}

