#include "stdafx.h"

// ********************************************
// xbee circular queues

int
rbt_xbee_create_frame_queue(
    uint32_t size,
    rbt_xbee_frame_queue_t *queue
)
{
    int error = 0;
    rbt_xbee_frame_t *frame_queue = 0;
    pthread_mutex_t mutex;
    pthread_cond_t cond;
    
    memset(&mutex, 0, sizeof mutex);
    memset(&cond, 0, sizeof cond);
    
    if (queue)
    {
        memset(queue, 0, sizeof *queue);
    }
    
    if (!size || !queue)
    {
        error = -EINVAL;
        goto quit;
    }
    
    error = -pthread_mutex_init(&mutex, 0);
    if (error)
    {
        goto quit;
    }
    
    error = -pthread_cond_init(&cond, 0);
    if (error)
    {
        goto quit;
    }

    frame_queue = (rbt_xbee_frame_t *)malloc(size * sizeof(rbt_xbee_frame_t));
    if (!frame_queue)
    {
        error = -errno;
        goto quit;
    }
    memset(frame_queue, 0, size * sizeof(rbt_xbee_frame_t));

    // no failure
    queue->size = size;
    queue->queue = frame_queue;
    frame_queue = 0;
    queue->mutex = mutex;
    memset(&mutex, 0, sizeof mutex);
    queue->cond = cond;
    memset(&cond, 0, sizeof cond);
    error = 0;

quit:

    pthread_mutex_destroy(&mutex);
    pthread_cond_destroy(&cond);
    free(frame_queue);

    return error;
}

void
rbt_xbee_free_frame_queue(
    rbt_xbee_frame_queue_t *queue
)
{
    if (!queue)
    {
        return;
    }

    while (queue->length > 0)
    {
        assert(queue->head && queue->tail);
        rbt_xbee_free_frame(queue->head);
        --queue->length;

        if (queue->head == queue->tail)
        {
            assert(queue->length == 0);
            queue->head = 0;
            queue->tail = 0;
        }
        else
        {
            assert(queue->length > 0);
            ++queue->head;
            if (queue->head >= queue->queue + queue->size)
            {
                queue->head = queue->queue;
            }
        }
    }

    assert(!queue->head && !queue->tail);
    
    pthread_mutex_destroy(&queue->mutex);
    pthread_cond_destroy(&queue->cond);
    free(queue->queue);

    memset(queue, 0, sizeof *queue);
}

int
rbt_xbee_enqueue_frame(
    rbt_xbee_frame_queue_t *queue,
    rbt_xbee_frame_t *frame
)
{
    int error = 0;
    int locked = 0;

    if (!queue || !queue->size || !frame)
    {
        error = -EINVAL;
        goto quit;
    }

    // lock the queue
    error = -pthread_mutex_lock(&queue->mutex);
    if (error)
    {
        goto quit;
    }
    locked = 1;

    // is queue full?
    if (queue->length >= queue->size)
    {
        error = -ENOBUFS;
        goto quit;
    }
    
    // signal may error
    error = -pthread_cond_signal(&queue->cond);
    if (error)
    {
        goto quit;
    }

    // no failure
    if (queue->tail)
    {
        assert(queue->head && queue->length);
        ++queue->length;
        ++queue->tail;
        if (queue->tail >= queue->queue + queue->size)
        {
            queue->tail = queue->queue;
        }
    }
    else
    {
        assert(!queue->head && !queue->length);
        ++queue->length;
        queue->head = queue->queue;
        queue->tail = queue->queue;
    }

    *(queue->tail) = *frame;
    memset(frame, 0, sizeof *frame);

quit:

    if (locked)
    {
        pthread_mutex_unlock(&queue->mutex);
    }

    return error;
}

int
rbt_xbee_dequeue_frame(
    rbt_xbee_frame_queue_t *queue,
    rbt_xbee_frame_t *frame,
    uint32_t timeout
)
{
    int error = 0;
    int locked = 0;
    struct timespec wait_timeout = {0, 0};
    
    if (frame)
    {
        memset(frame, 0, sizeof *frame);
    }

    if (!queue || !queue->size || !frame)
    {
        error = -EINVAL;
        goto quit;
    }

    // lock the queue
    error = -pthread_mutex_lock(&queue->mutex);
    if (error)
    {
        goto quit;
    }
    locked = 1;

    // is queue empty?
    while (!queue->length)
    {
        // wait for item to be enqueued
        if (timeout)
        {
            if (clock_gettime(CLOCK_REALTIME, &wait_timeout))
            {
                error = -errno;
                goto quit;
            }

            wait_timeout.tv_sec += timeout;

            error = -pthread_cond_timedwait(&queue->cond, &queue->mutex, &wait_timeout);
        }
        else
        {
            error = -pthread_cond_wait(&queue->cond, &queue->mutex);
        }

        if (error)
        {
            goto quit;
        }
    }
    assert(queue->length && queue->head && queue->tail);

    // no failure
    *frame = *(queue->head);
    memset(queue->head, 0, sizeof *(queue->head));
    --queue->length;

    if (queue->head == queue->tail)
    {
        assert(queue->length == 0);
        queue->head = 0;
        queue->tail = 0;
    }
    else
    {
        assert(queue->length > 0);
        ++queue->head;
        if (queue->head >= queue->queue + queue->size)
        {
            queue->head = queue->queue;
        }
    }

quit:

    if (locked)
    {
        pthread_mutex_unlock(&queue->mutex);
    }

    return error;
}

