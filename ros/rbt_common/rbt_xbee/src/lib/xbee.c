#include "stdafx.h"

// ********************************************
// private handlers

void
rbt_xbee_handle_receive_packet_frame(
    rbt_xbee_t *xbee,
    const rbt_xbee_frame_t *frame
)
{
    rbt_xbee_packet_t packet;
    uint8_t flags = 0;
    rbt_xbee_receive_callback_t receive_callback = 0;
    void *receive_callback_context = 0;

    if (rbt_xbee_decode_packet(frame, &packet, &flags))
    {
        return;
    }
    
    if (pthread_mutex_lock(&xbee->mutex))
    {
        return;
    }
    
    receive_callback = xbee->receive_callback;
    receive_callback_context = xbee->receive_callback_context;
    
    pthread_mutex_unlock(&xbee->mutex);

    if (receive_callback)
    {
        receive_callback(receive_callback_context, &packet, flags);
    }

    rbt_xbee_free_packet(&packet);
}

void
rbt_xbee_handle_transmit_status_frame(
    rbt_xbee_t *xbee,
    const rbt_xbee_frame_t *frame
)
{
    uint8_t id = 0;
    rbt_xbee_addr16_t addr16 = {0};
    uint8_t retries = 0;
    uint8_t delivery_status = 0;
    uint8_t discovery_status = 0;
    rbt_xbee_transmit_callback_t transmit_callback = 0;
    void *transmit_callback_context = 0;

    if (rbt_xbee_decode_transmit_status(frame,
                                        &id,
                                        &addr16,
                                        &retries,
                                        &delivery_status,
                                        &discovery_status))
    {
        return;
    }
    assert(id > 0);
    
    if (pthread_mutex_lock(&xbee->mutex))
    {
        return;
    }

    if (xbee->transmit_callback[id])
    {
        transmit_callback = xbee->transmit_callback[id];
        xbee->transmit_callback[id] = 0;
        transmit_callback_context = xbee->transmit_callback_context[id];
        xbee->transmit_callback_context[id] = 0;
    }

    pthread_mutex_unlock(&xbee->mutex);

    if (transmit_callback)
    {
        transmit_callback(transmit_callback_context,
                          id,
                          addr16,
                          retries,
                          delivery_status,
                          discovery_status);
    }
}

// ********************************************
// xbee threads

void *
rbt_xbee_frame_transmit_thread(
    void *arg
)
{
    int error = 0;
    rbt_xbee_t *xbee = (rbt_xbee_t *)arg;
    rbt_xbee_frame_t frame;
    fd_set write_fd_set;
    struct timeval write_timeout;
    int write_select = 0;
    
    FD_ZERO(&write_fd_set);
    memset(&write_timeout, 0, sizeof write_timeout);
    memset(&frame, 0, sizeof frame);

    if (!xbee)
    {
        error = -EINVAL;
        goto quit;
    }

    while (xbee->state == rbt_xbee_state_running)
    {    
        rbt_xbee_free_frame(&frame);
        error = rbt_xbee_dequeue_frame(&xbee->frame_transmit_queue, &frame, 3);
        if (error == -ETIMEDOUT)
        {
            continue;
        }

        if (error)
        {
            goto quit;
        }

        error = rbt_xbee_transmit_frame(&xbee->device, &frame);
        if (error == 0)
        {
            // frame transmitted completely
            // grab next from queue
            continue;
        }
        
        if (error != -EWOULDBLOCK)
        {
            goto quit;
        }
        
        // frame transmit would block
        while (xbee->state == rbt_xbee_state_running)
        {
            FD_ZERO(&write_fd_set);
            FD_SET(xbee->device.fd, &write_fd_set);

            write_timeout.tv_sec = 3;
            write_timeout.tv_usec = 0;

            write_select = select(xbee->device.fd + 1, 0, &write_fd_set, 0, &write_timeout);
            if (write_select < 0)
            {
                error = -errno;
                goto quit;
            }
                
            if (write_select == 0)
            {
                continue;
            }

            error = rbt_xbee_transmit_frame(&xbee->device, 0);
            if (error == 0)
            {
                // frame transmitted completely
                // grab next in queue
                break;
            }
            
            if (error != -EWOULDBLOCK)
            {
                goto quit;
            }
        }
    }

quit:

    rbt_xbee_free_frame(&frame);

    return (void *)(size_t)error;
}

void *
rbt_xbee_frame_receive_thread(
    void *arg
)
{
    int error = 0;
    rbt_xbee_t *xbee = (rbt_xbee_t *)arg;
    fd_set read_fd_set;
    struct timeval read_timeout;
    int read_select = 0;
    rbt_xbee_frame_t frame;
    
    FD_ZERO(&read_fd_set);
    memset(&read_timeout, 0, sizeof read_timeout);
    memset(&frame, 0, sizeof frame);
    
    if (!xbee)
    {
        error = -EINVAL;
        goto quit;
    }

    while (xbee->state == rbt_xbee_state_running)
    {
        rbt_xbee_free_frame(&frame);
        error = rbt_xbee_receive_frame(&xbee->device, &frame);
        if (error == 0)
        {
            error = rbt_xbee_enqueue_frame(&xbee->frame_receive_queue, &frame);
            if (error)
            {
                goto quit;
            }
            continue;
        }

        if (error != -EWOULDBLOCK)
        {
            goto quit;
        }

        while (xbee->state == rbt_xbee_state_running)
        {
            FD_ZERO(&read_fd_set);
            FD_SET(xbee->device.fd, &read_fd_set);

            read_timeout.tv_sec = 3;
            read_timeout.tv_usec = 0;

            read_select = select(xbee->device.fd + 1, &read_fd_set, 0, 0, &read_timeout);
            if (read_select < 0)
            {
                error = -errno;
                goto quit;
            }

            if (read_select > 0)
            {
                break;
            }
        }
    }
    
quit:

    rbt_xbee_free_frame(&frame);

    return (void *)(size_t)error;
}

void *
rbt_xbee_dispatch_thread(
    void *arg
)
{
    int error = 0;
    rbt_xbee_t *xbee = (rbt_xbee_t *)arg;
    rbt_xbee_frame_t frame;

    memset(&frame, 0, sizeof frame);

    if (!xbee)
    {
        error = -EINVAL;
        goto quit;
    }
    
    while (xbee->state == rbt_xbee_state_running)
    {
        rbt_xbee_free_frame(&frame);
        error = rbt_xbee_dequeue_frame(&xbee->frame_receive_queue, &frame, 3);
        if (error == -ETIMEDOUT)
        {
            continue;
        }

        if (error)
        {
            goto quit;
        }
        
        switch (frame.type)
        {
        case RBT_XBEE_ZIGBEE_RECEIVE_PACKET_FRAME:
            rbt_xbee_handle_receive_packet_frame(xbee, &frame);
            break;
        case RBT_XBEE_ZIGBEE_TRANSMIT_STATUS_FRAME:
            rbt_xbee_handle_transmit_status_frame(xbee, &frame);
            break;
        }
    }

quit:

    rbt_xbee_free_frame(&frame);

    return (void *)(size_t)error;
}

// ********************************************
// xbee

int
rbt_xbee_start(
    const char* port,
    speed_t baudrate,
    uint32_t dispatch_threads_count,
    rbt_xbee_t *xbee
)
{
    const uint32_t default_frame_transmit_queue_size = 256;
    const uint32_t default_frame_receive_queue_size = 256;

    int error = 0;
    uint32_t i = 0;

    if (xbee)
    {
        memset(xbee, 0, sizeof *xbee);
        xbee->device = RBT_XBEE_DEVICE_INITIALIZER;
    }
    
    if (!port || !dispatch_threads_count || !xbee)
    {
        error = -EINVAL;
        goto quit;
    }
    
    xbee->state = rbt_xbee_state_running;
    
    // create queues
    error = rbt_xbee_create_frame_queue(default_frame_transmit_queue_size, &xbee->frame_transmit_queue);
    if (error)
    {
        goto quit;
    }

    error = rbt_xbee_create_frame_queue(default_frame_receive_queue_size, &xbee->frame_receive_queue);
    if (error)
    {
        goto quit;
    }

    // open device
    error = rbt_xbee_open_device(port, baudrate, &xbee->device);
    if (error < 0)
    {
        goto quit;
    }
    
    // put device into non blocking mode
    if (fcntl(xbee->device.fd, F_SETFL, fcntl(xbee->device.fd, F_GETFL) | O_NONBLOCK))
    {
        error = -errno;
        goto quit;
    }
    
    // create mutex
    error = -pthread_mutex_init(&xbee->mutex, 0);
    if (error)
    {
        goto quit;
    }

    // create threads
    error = -pthread_create(&xbee->frame_transmit_thread, 0, rbt_xbee_frame_transmit_thread, xbee);
    if (error)
    {
        goto quit;
    }

    error = -pthread_create(&xbee->frame_receive_thread, 0, rbt_xbee_frame_receive_thread, xbee);
    if (error)
    {
        goto quit;
    }
    
    // create dispatch threads
    xbee->dispatch_threads = (pthread_t *)malloc(dispatch_threads_count * sizeof(pthread_t));
    if (!xbee->dispatch_threads)
    {
        error = -errno;
        goto quit;
    }
    memset(xbee->dispatch_threads, 0, dispatch_threads_count * sizeof(pthread_t));
    xbee->dispatch_threads_count = dispatch_threads_count;

    for (i = 0; i < xbee->dispatch_threads_count; i++)
    {
        error = -pthread_create(&xbee->dispatch_threads[i], 0, rbt_xbee_dispatch_thread, xbee);
        if (error)
        {
            goto quit;
        }
    }

quit:

    if (error)
    {
        rbt_xbee_stop(xbee);
    }

    return error;
}

void
rbt_xbee_stop(
    rbt_xbee_t *xbee
)
{
    uint32_t i = 0;

    if (!xbee)
    {
        return;
    }

    xbee->state = rbt_xbee_state_terminated;
    
    pthread_join(xbee->frame_transmit_thread, 0);
    pthread_join(xbee->frame_receive_thread, 0);
    for (i = 0; i < xbee->dispatch_threads_count; i++)
    {
        pthread_join(xbee->dispatch_threads[i], 0);
    }

    rbt_xbee_close_device(&xbee->device);

    rbt_xbee_free_frame_queue(&xbee->frame_transmit_queue);
    rbt_xbee_free_frame_queue(&xbee->frame_receive_queue);
    
    free(xbee->dispatch_threads);
    
    pthread_mutex_destroy(&xbee->mutex);

    memset(xbee, 0, sizeof *xbee);
    xbee->device = RBT_XBEE_DEVICE_INITIALIZER;
}

int
rbt_xbee_set_receive_callback(
    rbt_xbee_t *xbee,
    rbt_xbee_receive_callback_t receive_callback,
    void *receive_callback_context
)
{
    int error = 0;
    int locked = 0;

    if (!xbee || xbee->state != rbt_xbee_state_running || !receive_callback)
    {
        error = -EINVAL;
        goto quit;
    }

    error = -pthread_mutex_lock(&xbee->mutex);
    if (error)
    {
        goto quit;
    }
    locked = 1;

    xbee->receive_callback = receive_callback;
    xbee->receive_callback_context = receive_callback_context;

quit:

    if (locked)
    {
        pthread_mutex_unlock(&xbee->mutex);
    }

    return error;
}

int
rbt_xbee_transmit(
    rbt_xbee_t *xbee,
    const rbt_xbee_packet_t *packet,
    uint8_t hops,
    uint8_t options,
    rbt_xbee_transmit_callback_t callback,
    void *context,
    uint8_t *id
)
{
    int error = 0;
    int locked = 0;
    uint8_t next_id = 0;
    rbt_xbee_frame_t frame;

    memset(&frame, 0, sizeof frame);
    
    if (id)
    {
        *id = 0;
    }

    if (!xbee ||
        xbee->state != rbt_xbee_state_running ||
        !packet ||
        !packet->data ||
        !packet->length ||
        packet->length > RBT_XBEE_MAX_PACKET_LENGTH ||
        hops > RBT_XBEE_MAX_HOPS ||
        (id && !callback))
    {
        error = -EINVAL;
        goto quit;
    }

    if (id)
    {
        // callback needed
        // find an available id
        error = -pthread_mutex_lock(&xbee->mutex);
        if (error)
        {
            goto quit;
        }
        locked = 1;

        next_id = xbee->transmit_callback_id + 1;

        for (;;)
        {
            if (!next_id)
            {
                next_id = 1;
            }

            if (!xbee->transmit_callback[next_id])
            {
                assert(!xbee->transmit_callback_context[next_id]);
                break;
            }

            ++next_id;
            if (next_id == xbee->transmit_callback_id + 1)
            {
                // all id exhausted
                error = -ENOBUFS;
                goto quit;
            }
        }        
    }
    
    error = rbt_xbee_encode_packet(packet, next_id, hops, options, &frame);
    if (error)
    {
        goto quit;
    }

    error = rbt_xbee_enqueue_frame(&xbee->frame_transmit_queue, &frame);
    if (error)
    {
        goto quit;
    }
    
    // no failure
    if (next_id)
    {
        xbee->transmit_callback_id = next_id;
        xbee->transmit_callback[next_id] = callback;
        xbee->transmit_callback_context[next_id] = context;
        *id = next_id;
    }

quit:

    if (locked)
    {
        pthread_mutex_unlock(&xbee->mutex);
    }

    rbt_xbee_free_frame(&frame);

    return error;
}

