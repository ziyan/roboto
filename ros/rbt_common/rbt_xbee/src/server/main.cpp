#include "ros/ros.h"
#include "rbt_xbee/Packet.h"
#include "rbt_xbee/Transmit.h"
#include "rbt_xbee/rbt_xbee.h"
#include "rbt_xbee/rbt_xbee_network.h"

#include <errno.h>
#include <ros/console.h>

rbt_xbee_t xbee;
rbt_xbee_network_t network;

ros::ServiceServer service;
ros::Publisher publisher;

bool
transmit(
    rbt_xbee::Transmit::Request &req,
    rbt_xbee::Transmit::Response &res
)
{
    int error = 0;

    error = rbt_xbee_network_unicast(&network,
                                     RBT_XBEE_NETWORK_PROTOCOL_DATA,
                                     req.destination,
                                     &req.data[0],
                                     req.data.size());
    if (error)
    {
        goto quit;
    }
    
    ROS_INFO("rbt_xbee: sent data to %c (0x%02x), length %d", req.destination, req.destination, (int)req.data.size());

quit:

    return error == 0;
}

void
received(
    void *context,
    uint8_t protocol,
    uint8_t destination,
    uint8_t source,
    const uint8_t *data,
    uint8_t length
)
{
    rbt_xbee::Packet packet;

    packet.source = source;
    packet.data = std::vector<uint8_t>(length);
    memcpy(&packet.data[0], data, length);

    publisher.publish(packet);
    
    ROS_INFO("rbt_xbee: received data from %c (0x%02x), length %d", source, source, length);
}

void
heartbeat(
    void *context,
    uint8_t protocol,
    uint8_t destination,
    uint8_t source,
    const uint8_t *data,
    uint8_t length
)
{
    struct timeval tv;
    struct _heartbeat {
        uint64_t request;
        uint64_t response;
    } heartbeat;

    if (length == sizeof(uint64_t))
    {
        // reply with the current time
        heartbeat.request = *((uint64_t *)data);
        gettimeofday(&tv, 0);
        heartbeat.response = ((uint64_t)tv.tv_sec * 1000000u + tv.tv_usec) / 1000u;

        rbt_xbee_network_unicast(&network,
                                 RBT_XBEE_NETWORK_PROTOCOL_HEARTBEAT,
                                 source,
                                 (uint8_t *)&heartbeat,
                                 sizeof heartbeat);

        ROS_INFO("rbt_xbee: received heartbeat from %c (0x%02x), length %d, time difference %ld", source, source, length, heartbeat.response - heartbeat.request);
    }
    else
    {
        ROS_INFO("rbt_xbee: received heartbeat from %c (0x%02x), length %d", source, source, length);
    }
}

int main(int argc, char **argv)
{
    int error = 0;
    std::string port;
    int baudrate = 0;
    speed_t speed = B9600;
    std::string address;

    memset(&xbee, 0, sizeof xbee);
    xbee.device = RBT_XBEE_DEVICE_INITIALIZER;
    memset(&network, 0, sizeof network);

    ros::init(argc, argv, "rbt_xbee_server", ros::init_options::AnonymousName);
    ros::NodeHandle node;

    service = node.advertiseService("rbt_xbee_transmit", transmit);
    publisher = node.advertise<rbt_xbee::Packet>("rbt_xbee_receive", 1000);

    // get xbee parameters
    ros::param::param<std::string>("~address", address, "S");
    if (address.length() != 1)
    {
        error = -EINVAL;
        goto quit;
    }
    ROS_INFO("rbt_xbee: address = %c (0x%02x)", address.c_str()[0], address.c_str()[0]);

    ros::param::param<std::string>("~port", port, "/dev/ttyUSB0");
    if (port.length() == 0)
    {
        error = -EINVAL;
        goto quit;
    }
    ROS_INFO("rbt_xbee: port = %s", port.c_str());

    ros::param::param<int>("~baudrate", baudrate, 9600);
    switch (baudrate)
    {
    case 4800:
        speed = B4800;
        break;
    case 9600:
        speed = B9600;
        break;
    case 19200:
        speed = B19200;
        break;
    case 38400:
        speed = B38400;
        break;
    case 57600:
        speed = B57600;
        break;
    case 115200:
        speed = B115200;
        break;
    default:
        error = -EINVAL;
        goto quit;
    }
    ROS_INFO("rbt_xbee: baudrate = %d", baudrate);

    // initialize xbee
    error = rbt_xbee_start(port.c_str(), speed, 4, &xbee);
    if (error)
    {
        goto quit;
    }

    error = rbt_xbee_network_start(address.c_str()[0], &xbee, &network);
    if (error)
    {
        goto quit;
    }

    error = rbt_xbee_network_set_protocol_handler(&network, RBT_XBEE_NETWORK_PROTOCOL_DATA, received);
    if (error)
    {
        goto quit;
    }

    error = rbt_xbee_network_set_protocol_handler(&network, RBT_XBEE_NETWORK_PROTOCOL_HEARTBEAT, heartbeat);
    if (error)
    {
        goto quit;
    }
    
    ROS_INFO("rbt_xbee: initialized");

    ros::spin();

quit:

    if (error)
    {
        ROS_ERROR("rbt_xbee: exiting with error %d (0x%08x)", error, error);
    }
    else
    {
        ROS_INFO("rbt_xbee: exiting");
    }

    rbt_xbee_network_stop(&network);
    rbt_xbee_stop(&xbee);

    return error;
}
