#!/usr/bin/env python

import roslib
roslib.load_manifest('rbt_home_server')

import rospy
from rbt_xbee.msg import Packet
from rbt_home_server.server import Server

def main():
    server = Server()
    rospy.init_node('rbt_home_server', anonymous=True)
    rospy.Subscriber("rbt_xbee_receive", Packet, server.packet_receive_callback)
    rospy.spin()

if __name__ == '__main__':
    main()
