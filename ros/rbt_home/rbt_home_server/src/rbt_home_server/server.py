#!/usr/bin/env python

import roslib
roslib.load_manifest('rbt_home_server')

from rbt_xbee.msg import Packet
from struct import unpack
from datetime import datetime, timedelta
from dateutil import tz

class Server(object):

    def __init__(self):
        self.packet_handlers = {
            'c': self.current_packet_handler,
            'p': self.pump_packet_handler,
        }

    def current_packet_handler(self, packet):
        '''This function handles data packet coming from the current sensor'''

        assert(packet.data[0] == 'c')
        if len(packet.data) < 6 or len(packet.data) % 2 == 1:
            raise TypeError('Invalid packet received')

        (index,) = unpack('B', packet.data[1])

        (timestamp,) = unpack('<Q', packet.data[2:10])
        timestamp = datetime.utcfromtimestamp(timestamp / 1000.0).replace(tzinfo=tz.gettz('UTC'))

        currents = []
        for i in range(10, len(packet.data), 2):
            (current,) = unpack('<H', packet.data[i:i+2])
            currents.append(current / 1024.0 * 30.0)

        timestamps = []
        for i in range(len(currents)):
            timestamps.insert(0, timestamp - timedelta(seconds=i))

        data = zip(timestamps, currents)

        localtime = timestamp.astimezone(tz.gettz('America/Vancouver'))
        print "[%s] Current sensor #%d: %s" % (localtime, index, currents)


    def pump_packet_handler(self, packet):
        assert(packet.data[0] == 'p')
        if len(packet.data) != 3:
            raise TypeError('Invalid packet received')

        (index,) = unpack('B', packet.data[1])
        (state,) = unpack('B', packet.data[2])
        print "Pump #%d state: %d" % (index, state)


    def packet_receive_callback(self, packet):
        if len(packet.data) < 1:
            raise TypeError('Invalid packet received')

        packet_type = packet.data[0]

        if not packet_type in self.packet_handlers.keys():
            raise TypeError('Unable to handle packet of type %s' % repr(packet_type))

        self.packet_handlers[packet_type](packet)
